# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'InterfaceBrand'
        db.create_table('cotizador_interfacebrand', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['InterfaceBrand'])

        # Adding model 'InterfaceVideoPort'
        db.create_table('cotizador_interfacevideoport', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['InterfaceVideoPort'])

        # Adding model 'InterfaceCardBusName'
        db.create_table('cotizador_interfacecardbusname', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('show_version_and_lanes', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('cotizador', ['InterfaceCardBusName'])

        # Adding model 'InterfaceCardBusLane'
        db.create_table('cotizador_interfacecardbuslane', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['InterfaceCardBusLane'])

        # Adding model 'InterfaceCardBus'
        db.create_table('cotizador_interfacecardbus', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceCardBusName'])),
            ('lane', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceCardBusLane'])),
            ('version', self.gf('django.db.models.fields.DecimalField')(max_digits=2, decimal_places=1)),
        ))
        db.send_create_signal('cotizador', ['InterfaceCardBus'])

        # Adding model 'InterfaceSocketBrand'
        db.create_table('cotizador_interfacesocketbrand', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceBrand'])),
        ))
        db.send_create_signal('cotizador', ['InterfaceSocketBrand'])

        # Adding model 'InterfaceSocket'
        db.create_table('cotizador_interfacesocket', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('num_pins', self.gf('django.db.models.fields.IntegerField')()),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceSocketBrand'])),
        ))
        db.send_create_signal('cotizador', ['InterfaceSocket'])

        # Adding model 'InterfacePort'
        db.create_table('cotizador_interfaceport', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['InterfacePort'])

        # Adding model 'InterfaceMemoryFormat'
        db.create_table('cotizador_interfacememoryformat', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['InterfaceMemoryFormat'])

        # Adding model 'InterfaceMemoryType'
        db.create_table('cotizador_interfacememorytype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['InterfaceMemoryType'])

        # Adding model 'InterfaceMemoryBus'
        db.create_table('cotizador_interfacememorybus', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('format', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceMemoryFormat'])),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceMemoryType'])),
            ('pincount', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['InterfaceMemoryBus'])

        # Adding model 'InterfaceBus'
        db.create_table('cotizador_interfacebus', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['InterfaceBus'])

        # Adding model 'InterfacePowerConnector'
        db.create_table('cotizador_interfacepowerconnector', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['InterfacePowerConnector'])

        # Adding model 'InterfaceMotherboardFormat'
        db.create_table('cotizador_interfacemotherboardformat', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('width', self.gf('django.db.models.fields.IntegerField')()),
            ('height', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['InterfaceMotherboardFormat'])

        # Adding model 'ProductType'
        db.create_table('cotizador_producttype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('classname', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('urlname', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('displayname', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('adminurlname', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('indexname', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('ordering', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['ProductType'])

        # Adding model 'NotebookProcessorBrand'
        db.create_table('cotizador_notebookprocessorbrand', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceBrand'])),
        ))
        db.send_create_signal('cotizador', ['NotebookProcessorBrand'])

        # Adding model 'NotebookProcessorLineFamily'
        db.create_table('cotizador_notebookprocessorlinefamily', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookProcessorBrand'])),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('picture', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
        ))
        db.send_create_signal('cotizador', ['NotebookProcessorLineFamily'])

        # Adding model 'NotebookProcessorLine'
        db.create_table('cotizador_notebookprocessorline', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('family', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookProcessorLineFamily'])),
        ))
        db.send_create_signal('cotizador', ['NotebookProcessorLine'])

        # Adding model 'NotebookProcessorFrequency'
        db.create_table('cotizador_notebookprocessorfrequency', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['NotebookProcessorFrequency'])

        # Adding model 'NotebookProcessorCache'
        db.create_table('cotizador_notebookprocessorcache', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['NotebookProcessorCache'])

        # Adding model 'NotebookProcessorFSB'
        db.create_table('cotizador_notebookprocessorfsb', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['NotebookProcessorFSB'])

        # Adding model 'NotebookProcessorMultiplier'
        db.create_table('cotizador_notebookprocessormultiplier', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.DecimalField')(max_digits=3, decimal_places=1)),
        ))
        db.send_create_signal('cotizador', ['NotebookProcessorMultiplier'])

        # Adding model 'NotebookProcessorSocket'
        db.create_table('cotizador_notebookprocessorsocket', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('pincount', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['NotebookProcessorSocket'])

        # Adding model 'NotebookProcessorManufacturing'
        db.create_table('cotizador_notebookprocessormanufacturing', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['NotebookProcessorManufacturing'])

        # Adding model 'NotebookProcessorFamily'
        db.create_table('cotizador_notebookprocessorfamily', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('nm', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookProcessorManufacturing'])),
        ))
        db.send_create_signal('cotizador', ['NotebookProcessorFamily'])

        # Adding model 'NotebookProcessor'
        db.create_table('cotizador_notebookprocessor', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('min_voltage', self.gf('django.db.models.fields.IntegerField')()),
            ('max_voltage', self.gf('django.db.models.fields.IntegerField')()),
            ('core_number', self.gf('django.db.models.fields.IntegerField')()),
            ('tdp', self.gf('django.db.models.fields.DecimalField')(max_digits=4, decimal_places=1)),
            ('has_smp', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('has_turbo_mode', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('line', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookProcessorLine'])),
            ('frequency', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookProcessorFrequency'])),
            ('fsb', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookProcessorFSB'])),
            ('multiplier', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookProcessorMultiplier'])),
            ('cache', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookProcessorCache'])),
            ('socket', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookProcessorSocket'])),
            ('family', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookProcessorFamily'])),
            ('speed_score', self.gf('django.db.models.fields.IntegerField')()),
            ('consumption', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['NotebookProcessor'])

        # Adding model 'NotebookOpticalDrive'
        db.create_table('cotizador_notebookopticaldrive', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['NotebookOpticalDrive'])

        # Adding model 'NotebookBrand'
        db.create_table('cotizador_notebookbrand', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceBrand'])),
        ))
        db.send_create_signal('cotizador', ['NotebookBrand'])

        # Adding model 'NotebookLine'
        db.create_table('cotizador_notebookline', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookBrand'])),
        ))
        db.send_create_signal('cotizador', ['NotebookLine'])

        # Adding model 'NotebookLan'
        db.create_table('cotizador_notebooklan', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['NotebookLan'])

        # Adding model 'NotebookOperatingSystemBrand'
        db.create_table('cotizador_notebookoperatingsystembrand', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceBrand'])),
        ))
        db.send_create_signal('cotizador', ['NotebookOperatingSystemBrand'])

        # Adding model 'NotebookOperatingSystemFamily'
        db.create_table('cotizador_notebookoperatingsystemfamily', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookOperatingSystemBrand'])),
        ))
        db.send_create_signal('cotizador', ['NotebookOperatingSystemFamily'])

        # Adding model 'NotebookOperatingSystemLanguage'
        db.create_table('cotizador_notebookoperatingsystemlanguage', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['NotebookOperatingSystemLanguage'])

        # Adding model 'NotebookOperatingSystem'
        db.create_table('cotizador_notebookoperatingsystem', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('is_64_bit', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('family', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookOperatingSystemFamily'])),
            ('language', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookOperatingSystemLanguage'])),
        ))
        db.send_create_signal('cotizador', ['NotebookOperatingSystem'])

        # Adding model 'NotebookVideoCardMemory'
        db.create_table('cotizador_notebookvideocardmemory', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['NotebookVideoCardMemory'])

        # Adding model 'NotebookVideoCardType'
        db.create_table('cotizador_notebookvideocardtype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['NotebookVideoCardType'])

        # Adding model 'NotebookVideoCardBrand'
        db.create_table('cotizador_notebookvideocardbrand', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceBrand'])),
        ))
        db.send_create_signal('cotizador', ['NotebookVideoCardBrand'])

        # Adding model 'NotebookVideoCardLine'
        db.create_table('cotizador_notebookvideocardline', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookVideoCardBrand'])),
        ))
        db.send_create_signal('cotizador', ['NotebookVideoCardLine'])

        # Adding model 'NotebookVideoCard'
        db.create_table('cotizador_notebookvideocard', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('gpu_frequency', self.gf('django.db.models.fields.IntegerField')()),
            ('memory_frequency', self.gf('django.db.models.fields.IntegerField')()),
            ('card_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookVideoCardType'])),
            ('line', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookVideoCardLine'])),
            ('memory', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookVideoCardMemory'])),
            ('speed_score', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['NotebookVideoCard'])

        # Adding model 'NotebookWifiCardBrand'
        db.create_table('cotizador_notebookwificardbrand', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceBrand'])),
        ))
        db.send_create_signal('cotizador', ['NotebookWifiCardBrand'])

        # Adding model 'NotebookWifiCardNorm'
        db.create_table('cotizador_notebookwificardnorm', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['NotebookWifiCardNorm'])

        # Adding model 'NotebookWifiCard'
        db.create_table('cotizador_notebookwificard', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookWifiCardBrand'])),
            ('norm', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookWifiCardNorm'])),
        ))
        db.send_create_signal('cotizador', ['NotebookWifiCard'])

        # Adding model 'NotebookVideoPort'
        db.create_table('cotizador_notebookvideoport', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['NotebookVideoPort'])

        # Adding model 'NotebookScreenResolution'
        db.create_table('cotizador_notebookscreenresolution', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('horizontal', self.gf('django.db.models.fields.IntegerField')()),
            ('vertical', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['NotebookScreenResolution'])

        # Adding model 'NotebookScreenSizeFamily'
        db.create_table('cotizador_notebookscreensizefamily', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('base_size', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['NotebookScreenSizeFamily'])

        # Adding model 'NotebookScreenSize'
        db.create_table('cotizador_notebookscreensize', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('size', self.gf('django.db.models.fields.DecimalField')(max_digits=3, decimal_places=1)),
            ('family', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookScreenSizeFamily'])),
        ))
        db.send_create_signal('cotizador', ['NotebookScreenSize'])

        # Adding model 'NotebookScreen'
        db.create_table('cotizador_notebookscreen', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_glossy', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_touchscreen', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_led', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_rotating', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('resolution', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookScreenResolution'])),
            ('size', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookScreenSize'])),
        ))
        db.send_create_signal('cotizador', ['NotebookScreen'])

        # Adding model 'NotebookPowerAdapter'
        db.create_table('cotizador_notebookpoweradapter', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('power', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['NotebookPowerAdapter'])

        # Adding model 'NotebookStorageDriveType'
        db.create_table('cotizador_notebookstoragedrivetype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['NotebookStorageDriveType'])

        # Adding model 'NotebookStorageDriveCapacity'
        db.create_table('cotizador_notebookstoragedrivecapacity', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['NotebookStorageDriveCapacity'])

        # Adding model 'NotebookStorageDriveRpm'
        db.create_table('cotizador_notebookstoragedriverpm', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['NotebookStorageDriveRpm'])

        # Adding model 'NotebookStorageDrive'
        db.create_table('cotizador_notebookstoragedrive', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('read_speed', self.gf('django.db.models.fields.IntegerField')()),
            ('write_speed', self.gf('django.db.models.fields.IntegerField')()),
            ('drive_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookStorageDriveType'])),
            ('rpm', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookStorageDriveRpm'])),
            ('capacity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookStorageDriveCapacity'])),
        ))
        db.send_create_signal('cotizador', ['NotebookStorageDrive'])

        # Adding model 'NotebookChipsetBrand'
        db.create_table('cotizador_notebookchipsetbrand', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceBrand'])),
        ))
        db.send_create_signal('cotizador', ['NotebookChipsetBrand'])

        # Adding model 'NotebookChipset'
        db.create_table('cotizador_notebookchipset', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookChipsetBrand'])),
        ))
        db.send_create_signal('cotizador', ['NotebookChipset'])

        # Adding model 'NotebookRamQuantity'
        db.create_table('cotizador_notebookramquantity', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.DecimalField')(max_digits=3, decimal_places=1)),
        ))
        db.send_create_signal('cotizador', ['NotebookRamQuantity'])

        # Adding model 'NotebookRamType'
        db.create_table('cotizador_notebookramtype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['NotebookRamType'])

        # Adding model 'NotebookRamFrequency'
        db.create_table('cotizador_notebookramfrequency', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['NotebookRamFrequency'])

        # Adding model 'NotebookCardReader'
        db.create_table('cotizador_notebookcardreader', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['NotebookCardReader'])

        # Adding model 'Store'
        db.create_table('cotizador_store', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('classname', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('sponsor_cap', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('picture', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('affiliate_id', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal('cotizador', ['Store'])

        # Adding model 'NotebookType'
        db.create_table('cotizador_notebooktype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('css_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('ordering', self.gf('django.db.models.fields.IntegerField')()),
            ('screen_size', self.gf('django.db.models.fields.IntegerField')()),
            ('processor_speed', self.gf('django.db.models.fields.IntegerField')()),
            ('processor_consumption', self.gf('django.db.models.fields.IntegerField')()),
            ('video_card_speed', self.gf('django.db.models.fields.IntegerField')()),
            ('storage_quantity', self.gf('django.db.models.fields.IntegerField')()),
            ('ram_quantity', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['NotebookType'])

        # Adding model 'Product'
        db.create_table('cotizador_product', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('part_number', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('date_added', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('ptype', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ProductType'], null=True, blank=True)),
            ('shp', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='chosen_by', null=True, to=orm['cotizador.StoreHasProduct'])),
            ('sponsored_shp', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='sponsored_product', null=True, to=orm['cotizador.StoreHasProduct'])),
            ('week_visitor_count', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('week_discount', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('week_external_visits', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('long_description', self.gf('django.db.models.fields.TextField')(default=' ')),
            ('display_name', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('review_url', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('similar_products', self.gf('django.db.models.fields.CommaSeparatedIntegerField')(default='0', max_length=30)),
            ('created_by', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True, blank=True)),
            ('picture', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
        ))
        db.send_create_signal('cotizador', ['Product'])

        # Adding model 'Notebook'
        db.create_table('cotizador_notebook', (
            ('product_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cotizador.Product'], unique=True, primary_key=True)),
            ('is_ram_dual_channel', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('has_bluetooth', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('has_esata', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('has_fingerprint_reader', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('has_firewire', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('battery_mah', self.gf('django.db.models.fields.IntegerField')()),
            ('battery_mwh', self.gf('django.db.models.fields.IntegerField')()),
            ('battery_mv', self.gf('django.db.models.fields.IntegerField')()),
            ('battery_cells', self.gf('django.db.models.fields.IntegerField')()),
            ('weight', self.gf('django.db.models.fields.IntegerField')()),
            ('width', self.gf('django.db.models.fields.IntegerField')()),
            ('height', self.gf('django.db.models.fields.IntegerField')()),
            ('thickness', self.gf('django.db.models.fields.IntegerField')()),
            ('usb_port_count', self.gf('django.db.models.fields.IntegerField')()),
            ('webcam_mp', self.gf('django.db.models.fields.DecimalField')(max_digits=3, decimal_places=1)),
            ('ntype', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookType'])),
            ('line', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookLine'])),
            ('processor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookProcessor'])),
            ('lan', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookLan'])),
            ('screen', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookScreen'])),
            ('operating_system', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookOperatingSystem'])),
            ('ram_quantity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookRamQuantity'])),
            ('ram_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookRamType'])),
            ('ram_frequency', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookRamFrequency'])),
            ('chipset', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookChipset'])),
            ('optical_drive', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookOpticalDrive'])),
            ('wifi_card', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookWifiCard'])),
            ('power_adapter', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookPowerAdapter'])),
            ('card_reader', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.NotebookCardReader'])),
        ))
        db.send_create_signal('cotizador', ['Notebook'])

        # Adding M2M table for field video_card on 'Notebook'
        db.create_table('cotizador_notebook_video_card', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('notebook', models.ForeignKey(orm['cotizador.notebook'], null=False)),
            ('notebookvideocard', models.ForeignKey(orm['cotizador.notebookvideocard'], null=False))
        ))
        db.create_unique('cotizador_notebook_video_card', ['notebook_id', 'notebookvideocard_id'])

        # Adding M2M table for field video_port on 'Notebook'
        db.create_table('cotizador_notebook_video_port', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('notebook', models.ForeignKey(orm['cotizador.notebook'], null=False)),
            ('notebookvideoport', models.ForeignKey(orm['cotizador.notebookvideoport'], null=False))
        ))
        db.create_unique('cotizador_notebook_video_port', ['notebook_id', 'notebookvideoport_id'])

        # Adding M2M table for field storage_drive on 'Notebook'
        db.create_table('cotizador_notebook_storage_drive', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('notebook', models.ForeignKey(orm['cotizador.notebook'], null=False)),
            ('notebookstoragedrive', models.ForeignKey(orm['cotizador.notebookstoragedrive'], null=False))
        ))
        db.create_unique('cotizador_notebook_storage_drive', ['notebook_id', 'notebookstoragedrive_id'])

        # Adding model 'StoreHasProduct'
        db.create_table('cotizador_storehasproduct', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.Product'], null=True, blank=True)),
            ('shpe', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.StoreHasProductEntity'], null=True, blank=True)),
        ))
        db.send_create_signal('cotizador', ['StoreHasProduct'])

        # Adding model 'StoreHasProductEntity'
        db.create_table('cotizador_storehasproductentity', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('url', self.gf('django.db.models.fields.TextField')()),
            ('custom_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('part_number', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('is_available', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_hidden', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('latest_price', self.gf('django.db.models.fields.IntegerField')()),
            ('comparison_field', self.gf('django.db.models.fields.TextField')()),
            ('store', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.Store'])),
            ('prevent_availability_change', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('date_added', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('date_resolved', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('shp', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.StoreHasProduct'], null=True, blank=True)),
            ('ptype', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ProductType'], null=True, blank=True)),
            ('resolved_by', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True, blank=True)),
        ))
        db.send_create_signal('cotizador', ['StoreHasProductEntity'])

        # Adding model 'ProductPriceChange'
        db.create_table('cotizador_productpricechange', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('price', self.gf('django.db.models.fields.IntegerField')()),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('notebook', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.Product'])),
        ))
        db.send_create_signal('cotizador', ['ProductPriceChange'])

        # Adding model 'ProductComment'
        db.create_table('cotizador_productcomment', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('validated', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('comments', self.gf('django.db.models.fields.TextField')()),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('nickname', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True, blank=True)),
            ('notebook', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.Product'])),
        ))
        db.send_create_signal('cotizador', ['ProductComment'])

        # Adding model 'ProductPicture'
        db.create_table('cotizador_productpicture', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('picture', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('notebook', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.Product'])),
        ))
        db.send_create_signal('cotizador', ['ProductPicture'])

        # Adding model 'StoreProductHistory'
        db.create_table('cotizador_storeproducthistory', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('price', self.gf('django.db.models.fields.IntegerField')()),
            ('date', self.gf('django.db.models.fields.DateField')(db_index=True)),
            ('registry', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.StoreHasProductEntity'])),
        ))
        db.send_create_signal('cotizador', ['StoreProductHistory'])

        # Adding model 'LogEntry'
        db.create_table('cotizador_logentry', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('date', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal('cotizador', ['LogEntry'])

        # Adding model 'LogChangeEntityName'
        db.create_table('cotizador_logchangeentityname', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('entity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.StoreHasProductEntity'])),
            ('log_entry', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.LogEntry'])),
            ('old_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('new_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['LogChangeEntityName'])

        # Adding model 'LogEntryMessage'
        db.create_table('cotizador_logentrymessage', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('message', self.gf('django.db.models.fields.TextField')()),
            ('logEntry', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.LogEntry'])),
        ))
        db.send_create_signal('cotizador', ['LogEntryMessage'])

        # Adding model 'ExternalVisit'
        db.create_table('cotizador_externalvisit', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('date', self.gf('django.db.models.fields.DateField')(db_index=True)),
            ('shn', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.StoreHasProductEntity'])),
            ('price', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('cotizador', ['ExternalVisit'])

        # Adding model 'SponsoredVisit'
        db.create_table('cotizador_sponsoredvisit', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('date', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
            ('shp', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.StoreHasProduct'])),
        ))
        db.send_create_signal('cotizador', ['SponsoredVisit'])

        # Adding model 'AdvertisementPosition'
        db.create_table('cotizador_advertisementposition', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['AdvertisementPosition'])

        # Adding model 'Advertisement'
        db.create_table('cotizador_advertisement', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('embedding_html', self.gf('django.db.models.fields.TextField')()),
            ('position', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.AdvertisementPosition'])),
            ('target_url', self.gf('django.db.models.fields.TextField')()),
            ('impressions', self.gf('django.db.models.fields.IntegerField')()),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('store', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.Store'], null=True)),
        ))
        db.send_create_signal('cotizador', ['Advertisement'])

        # Adding model 'AdvertisementImpression'
        db.create_table('cotizador_advertisementimpression', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('advertisement', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.Advertisement'])),
            ('date', self.gf('django.db.models.fields.DateField')(auto_now_add=True, db_index=True, blank=True)),
        ))
        db.send_create_signal('cotizador', ['AdvertisementImpression'])

        # Adding model 'AdvertisementImpressionPerDay'
        db.create_table('cotizador_advertisementimpressionperday', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('advertisement', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.Advertisement'])),
            ('date', self.gf('django.db.models.fields.DateField')(db_index=True)),
            ('count', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['AdvertisementImpressionPerDay'])

        # Adding unique constraint on 'AdvertisementImpressionPerDay', fields ['advertisement', 'date']
        db.create_unique('cotizador_advertisementimpressionperday', ['advertisement_id', 'date'])

        # Adding model 'AdvertisementVisit'
        db.create_table('cotizador_advertisementvisit', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('referer_url', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('advertisement', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.Advertisement'])),
            ('date', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal('cotizador', ['AdvertisementVisit'])

        # Adding model 'SearchRegistry'
        db.create_table('cotizador_searchregistry', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('query', self.gf('django.db.models.fields.TextField')()),
            ('date', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal('cotizador', ['SearchRegistry'])

        # Adding model 'ProductSubscription'
        db.create_table('cotizador_productsubscription', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.Product'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('email_notifications', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('cotizador', ['ProductSubscription'])

        # Adding model 'UserProfile'
        db.create_table('cotizador_userprofile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
            ('confirmation_mails_sent', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('change_mails_sent', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('assigned_store', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.Store'], null=True, blank=True)),
            ('facebook_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('can_access_competitivity_report', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('can_use_extra_ordering_options', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('cotizador', ['UserProfile'])

        # Adding M2M table for field managed_product_types on 'UserProfile'
        db.create_table('cotizador_userprofile_managed_product_types', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('userprofile', models.ForeignKey(orm['cotizador.userprofile'], null=False)),
            ('producttype', models.ForeignKey(orm['cotizador.producttype'], null=False))
        ))
        db.create_unique('cotizador_userprofile_managed_product_types', ['userprofile_id', 'producttype_id'])

        # Adding model 'LogFetchStoreError'
        db.create_table('cotizador_logfetchstoreerror', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('store', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.Store'])),
            ('log_entry', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.LogEntry'])),
            ('message', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['LogFetchStoreError'])

        # Adding model 'LogNewEntity'
        db.create_table('cotizador_lognewentity', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('shpe', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.StoreHasProductEntity'])),
            ('log_entry', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.LogEntry'])),
        ))
        db.send_create_signal('cotizador', ['LogNewEntity'])

        # Adding model 'LogReviveEntity'
        db.create_table('cotizador_logreviveentity', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('shpe', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.StoreHasProductEntity'])),
            ('log_entry', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.LogEntry'])),
        ))
        db.send_create_signal('cotizador', ['LogReviveEntity'])

        # Adding model 'LogLostEntity'
        db.create_table('cotizador_loglostentity', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('shpe', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.StoreHasProductEntity'])),
            ('log_entry', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.LogEntry'])),
        ))
        db.send_create_signal('cotizador', ['LogLostEntity'])

        # Adding model 'LogChangeEntityPrice'
        db.create_table('cotizador_logchangeentityprice', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('shpe', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.StoreHasProductEntity'])),
            ('log_entry', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.LogEntry'])),
            ('old_price', self.gf('django.db.models.fields.IntegerField')()),
            ('new_price', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['LogChangeEntityPrice'])

        # Adding model 'LogReviveProduct'
        db.create_table('cotizador_logreviveproduct', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.Product'])),
            ('log_entry', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.LogEntry'])),
        ))
        db.send_create_signal('cotizador', ['LogReviveProduct'])

        # Adding model 'LogLostProduct'
        db.create_table('cotizador_loglostproduct', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.Product'])),
            ('log_entry', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.LogEntry'])),
        ))
        db.send_create_signal('cotizador', ['LogLostProduct'])

        # Adding model 'LogChangeProductPrice'
        db.create_table('cotizador_logchangeproductprice', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.Product'])),
            ('log_entry', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.LogEntry'])),
            ('old_price', self.gf('django.db.models.fields.IntegerField')()),
            ('new_price', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['LogChangeProductPrice'])

        # Adding model 'MailChangeProductPrice'
        db.create_table('cotizador_mailchangeproductprice', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('subscription', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ProductSubscription'])),
            ('log', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.LogChangeProductPrice'])),
            ('success', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('cotizador', ['MailChangeProductPrice'])

        # Adding model 'MailLostProduct'
        db.create_table('cotizador_maillostproduct', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('subscription', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ProductSubscription'])),
            ('log', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.LogLostProduct'])),
            ('success', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('cotizador', ['MailLostProduct'])

        # Adding model 'MailReviveProduct'
        db.create_table('cotizador_mailreviveproduct', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('subscription', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ProductSubscription'])),
            ('log', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.LogReviveProduct'])),
            ('success', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('cotizador', ['MailReviveProduct'])

        # Adding model 'ProductComparisonList'
        db.create_table('cotizador_productcomparisonlist', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('date_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal('cotizador', ['ProductComparisonList'])

        # Adding M2M table for field products on 'ProductComparisonList'
        db.create_table('cotizador_productcomparisonlist_products', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('productcomparisonlist', models.ForeignKey(orm['cotizador.productcomparisonlist'], null=False)),
            ('product', models.ForeignKey(orm['cotizador.product'], null=False))
        ))
        db.create_unique('cotizador_productcomparisonlist_products', ['productcomparisonlist_id', 'product_id'])

        # Adding model 'ProductVisit'
        db.create_table('cotizador_productvisit', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, db_index=True, blank=True)),
            ('notebook', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.Product'])),
        ))
        db.send_create_signal('cotizador', ['ProductVisit'])

        # Adding model 'StoreCustomUpdateRegistry'
        db.create_table('cotizador_storecustomupdateregistry', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('store', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.Store'])),
            ('start_datetime', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('end_datetime', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('status', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
        ))
        db.send_create_signal('cotizador', ['StoreCustomUpdateRegistry'])

        # Adding model 'VideoCardBrand'
        db.create_table('cotizador_videocardbrand', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceBrand'])),
        ))
        db.send_create_signal('cotizador', ['VideoCardBrand'])

        # Adding model 'VideoCardGpuBrand'
        db.create_table('cotizador_videocardgpubrand', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceBrand'])),
        ))
        db.send_create_signal('cotizador', ['VideoCardGpuBrand'])

        # Adding model 'VideoCardGpuFamily'
        db.create_table('cotizador_videocardgpufamily', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.VideoCardGpuBrand'])),
        ))
        db.send_create_signal('cotizador', ['VideoCardGpuFamily'])

        # Adding model 'VideoCardGpuLine'
        db.create_table('cotizador_videocardgpuline', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('family', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.VideoCardGpuFamily'])),
        ))
        db.send_create_signal('cotizador', ['VideoCardGpuLine'])

        # Adding model 'VideoCardGpuArchitecture'
        db.create_table('cotizador_videocardgpuarchitecture', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.VideoCardGpuBrand'])),
        ))
        db.send_create_signal('cotizador', ['VideoCardGpuArchitecture'])

        # Adding model 'VideoCardGpuCoreFamily'
        db.create_table('cotizador_videocardgpucorefamily', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('architecture', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.VideoCardGpuArchitecture'])),
        ))
        db.send_create_signal('cotizador', ['VideoCardGpuCoreFamily'])

        # Adding model 'VideoCardGpuCore'
        db.create_table('cotizador_videocardgpucore', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('family', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.VideoCardGpuCoreFamily'])),
        ))
        db.send_create_signal('cotizador', ['VideoCardGpuCore'])

        # Adding model 'VideoCardPort'
        db.create_table('cotizador_videocardport', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('port', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceVideoPort'])),
        ))
        db.send_create_signal('cotizador', ['VideoCardPort'])

        # Adding model 'VideoCardHasPort'
        db.create_table('cotizador_videocardhasport', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('quantity', self.gf('django.db.models.fields.IntegerField')()),
            ('port', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.VideoCardPort'])),
        ))
        db.send_create_signal('cotizador', ['VideoCardHasPort'])

        # Adding model 'VideoCardBus'
        db.create_table('cotizador_videocardbus', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('bus', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceCardBus'])),
        ))
        db.send_create_signal('cotizador', ['VideoCardBus'])

        # Adding model 'VideoCardGpuDirectxVersion'
        db.create_table('cotizador_videocardgpudirectxversion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.DecimalField')(max_digits=3, decimal_places=1)),
        ))
        db.send_create_signal('cotizador', ['VideoCardGpuDirectxVersion'])

        # Adding model 'VideoCardGpuOpenglVersion'
        db.create_table('cotizador_videocardgpuopenglversion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.DecimalField')(max_digits=2, decimal_places=1)),
        ))
        db.send_create_signal('cotizador', ['VideoCardGpuOpenglVersion'])

        # Adding model 'VideoCardRefrigeration'
        db.create_table('cotizador_videocardrefrigeration', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['VideoCardRefrigeration'])

        # Adding model 'VideoCardSlotType'
        db.create_table('cotizador_videocardslottype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['VideoCardSlotType'])

        # Adding model 'VideoCardProfile'
        db.create_table('cotizador_videocardprofile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['VideoCardProfile'])

        # Adding model 'VideoCardMemoryType'
        db.create_table('cotizador_videocardmemorytype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['VideoCardMemoryType'])

        # Adding model 'VideoCardMemoryQuantity'
        db.create_table('cotizador_videocardmemoryquantity', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['VideoCardMemoryQuantity'])

        # Adding model 'VideoCardMemoryBusWidth'
        db.create_table('cotizador_videocardmemorybuswidth', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['VideoCardMemoryBusWidth'])

        # Adding model 'VideoCardGpuCoreCount'
        db.create_table('cotizador_videocardgpucorecount', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['VideoCardGpuCoreCount'])

        # Adding model 'VideoCardPowerConnector'
        db.create_table('cotizador_videocardpowerconnector', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('connector', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfacePowerConnector'])),
        ))
        db.send_create_signal('cotizador', ['VideoCardPowerConnector'])

        # Adding model 'VideoCardHasPowerConnector'
        db.create_table('cotizador_videocardhaspowerconnector', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('quantity', self.gf('django.db.models.fields.IntegerField')()),
            ('connector', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.VideoCardPowerConnector'])),
        ))
        db.send_create_signal('cotizador', ['VideoCardHasPowerConnector'])

        # Adding model 'VideoCardGpuManufacturingProcess'
        db.create_table('cotizador_videocardgpumanufacturingprocess', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['VideoCardGpuManufacturingProcess'])

        # Adding model 'VideoCardGpu'
        db.create_table('cotizador_videocardgpu', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('tdmark_id', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('stream_processors', self.gf('django.db.models.fields.IntegerField')()),
            ('texture_units', self.gf('django.db.models.fields.IntegerField')()),
            ('rops', self.gf('django.db.models.fields.IntegerField')()),
            ('default_core_clock', self.gf('django.db.models.fields.IntegerField')()),
            ('default_shader_clock', self.gf('django.db.models.fields.IntegerField')()),
            ('default_memory_clock', self.gf('django.db.models.fields.IntegerField')()),
            ('transistor_count', self.gf('django.db.models.fields.IntegerField')()),
            ('tdp', self.gf('django.db.models.fields.IntegerField')()),
            ('tdmark_06_score', self.gf('django.db.models.fields.IntegerField')()),
            ('tdmark_vantage_score', self.gf('django.db.models.fields.IntegerField')()),
            ('tdmark_11_score', self.gf('django.db.models.fields.IntegerField')()),
            ('has_multi_gpu_support', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('manufacturing_process', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.VideoCardGpuManufacturingProcess'])),
            ('core', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.VideoCardGpuCore'])),
            ('line', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.VideoCardGpuLine'])),
            ('dx_version', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.VideoCardGpuDirectxVersion'])),
            ('ogl_version', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.VideoCardGpuOpenglVersion'])),
            ('core_count', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.VideoCardGpuCoreCount'])),
        ))
        db.send_create_signal('cotizador', ['VideoCardGpu'])

        # Adding M2M table for field power_conns on 'VideoCardGpu'
        db.create_table('cotizador_videocardgpu_power_conns', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('videocardgpu', models.ForeignKey(orm['cotizador.videocardgpu'], null=False)),
            ('videocardhaspowerconnector', models.ForeignKey(orm['cotizador.videocardhaspowerconnector'], null=False))
        ))
        db.create_unique('cotizador_videocardgpu_power_conns', ['videocardgpu_id', 'videocardhaspowerconnector_id'])

        # Adding model 'VideoCard'
        db.create_table('cotizador_videocard', (
            ('product_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cotizador.Product'], unique=True, primary_key=True)),
            ('core_clock', self.gf('django.db.models.fields.IntegerField')()),
            ('shader_clock', self.gf('django.db.models.fields.IntegerField')()),
            ('memory_clock', self.gf('django.db.models.fields.IntegerField')()),
            ('gpu', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.VideoCardGpu'])),
            ('memory_bus_width', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.VideoCardMemoryBusWidth'])),
            ('memory_quantity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.VideoCardMemoryQuantity'])),
            ('memory_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.VideoCardMemoryType'])),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.VideoCardBrand'])),
            ('bus', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.VideoCardBus'])),
            ('profile', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.VideoCardProfile'])),
            ('slot_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.VideoCardSlotType'])),
            ('refrigeration', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.VideoCardRefrigeration'])),
        ))
        db.send_create_signal('cotizador', ['VideoCard'])

        # Adding M2M table for field video_ports on 'VideoCard'
        db.create_table('cotizador_videocard_video_ports', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('videocard', models.ForeignKey(orm['cotizador.videocard'], null=False)),
            ('videocardhasport', models.ForeignKey(orm['cotizador.videocardhasport'], null=False))
        ))
        db.create_unique('cotizador_videocard_video_ports', ['videocard_id', 'videocardhasport_id'])

        # Adding model 'ProcessorBrand'
        db.create_table('cotizador_processorbrand', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceBrand'])),
        ))
        db.send_create_signal('cotizador', ['ProcessorBrand'])

        # Adding model 'ProcessorFamily'
        db.create_table('cotizador_processorfamily', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ProcessorBrand'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('separator', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['ProcessorFamily'])

        # Adding model 'ProcessorLine'
        db.create_table('cotizador_processorline', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('family', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ProcessorFamily'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['ProcessorLine'])

        # Adding model 'ProcessorL2CacheQuantity'
        db.create_table('cotizador_processorl2cachequantity', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['ProcessorL2CacheQuantity'])

        # Adding model 'ProcessorL3CacheQuantity'
        db.create_table('cotizador_processorl3cachequantity', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['ProcessorL3CacheQuantity'])

        # Adding model 'ProcessorL2Cache'
        db.create_table('cotizador_processorl2cache', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('quantity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ProcessorL2CacheQuantity'])),
            ('multiplier', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['ProcessorL2Cache'])

        # Adding model 'ProcessorL3Cache'
        db.create_table('cotizador_processorl3cache', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('quantity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ProcessorL3CacheQuantity'])),
            ('multiplier', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['ProcessorL3Cache'])

        # Adding model 'ProcessorSocket'
        db.create_table('cotizador_processorsocket', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('socket', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceSocket'])),
        ))
        db.send_create_signal('cotizador', ['ProcessorSocket'])

        # Adding model 'ProcessorCoreCount'
        db.create_table('cotizador_processorcorecount', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['ProcessorCoreCount'])

        # Adding model 'ProcessorArchitecture'
        db.create_table('cotizador_processorarchitecture', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ProcessorBrand'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('turbo_step', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['ProcessorArchitecture'])

        # Adding model 'ProcessorManufacturingProcess'
        db.create_table('cotizador_processormanufacturingprocess', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['ProcessorManufacturingProcess'])

        # Adding model 'ProcessorCore'
        db.create_table('cotizador_processorcore', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('architecture', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ProcessorArchitecture'])),
            ('manufacturing_process', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ProcessorManufacturingProcess'])),
        ))
        db.send_create_signal('cotizador', ['ProcessorCore'])

        # Adding model 'ProcessorMultiplier'
        db.create_table('cotizador_processormultiplier', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.DecimalField')(max_digits=3, decimal_places=1)),
        ))
        db.send_create_signal('cotizador', ['ProcessorMultiplier'])

        # Adding model 'ProcessorFsb'
        db.create_table('cotizador_processorfsb', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['ProcessorFsb'])

        # Adding model 'ProcessorGraphics'
        db.create_table('cotizador_processorgraphics', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['ProcessorGraphics'])

        # Adding model 'Processor'
        db.create_table('cotizador_processor', (
            ('product_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cotizador.Product'], unique=True, primary_key=True)),
            ('pcmark_id', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('frequency', self.gf('django.db.models.fields.IntegerField')()),
            ('tdp', self.gf('django.db.models.fields.IntegerField')()),
            ('min_voltage', self.gf('django.db.models.fields.IntegerField')()),
            ('max_voltage', self.gf('django.db.models.fields.IntegerField')()),
            ('pcmark_05_score', self.gf('django.db.models.fields.IntegerField')()),
            ('pcmark_vantage_score', self.gf('django.db.models.fields.IntegerField')()),
            ('passmark_score', self.gf('django.db.models.fields.IntegerField')()),
            ('is_64_bit', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('has_vt', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('has_smp', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('has_unlocked_multiplier', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('l2_cache', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ProcessorL2Cache'])),
            ('l3_cache', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ProcessorL3Cache'])),
            ('line', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ProcessorLine'])),
            ('socket', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ProcessorSocket'])),
            ('core_count', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ProcessorCoreCount'])),
            ('core', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ProcessorCore'])),
            ('multiplier', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ProcessorMultiplier'])),
            ('fsb', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ProcessorFsb'])),
            ('graphics', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ProcessorGraphics'])),
            ('turbo_modes', self.gf('django.db.models.fields.CommaSeparatedIntegerField')(max_length=50)),
        ))
        db.send_create_signal('cotizador', ['Processor'])

        # Adding model 'ScreenType'
        db.create_table('cotizador_screentype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['ScreenType'])

        # Adding model 'ScreenBrand'
        db.create_table('cotizador_screenbrand', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceBrand'])),
        ))
        db.send_create_signal('cotizador', ['ScreenBrand'])

        # Adding model 'ScreenLine'
        db.create_table('cotizador_screenline', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ScreenBrand'])),
        ))
        db.send_create_signal('cotizador', ['ScreenLine'])

        # Adding model 'ScreenDisplayType'
        db.create_table('cotizador_screendisplaytype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['ScreenDisplayType'])

        # Adding model 'ScreenDisplay'
        db.create_table('cotizador_screendisplay', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('backlight', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('dtype', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ScreenDisplayType'])),
        ))
        db.send_create_signal('cotizador', ['ScreenDisplay'])

        # Adding model 'ScreenSizeFamily'
        db.create_table('cotizador_screensizefamily', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['ScreenSizeFamily'])

        # Adding model 'ScreenSize'
        db.create_table('cotizador_screensize', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.DecimalField')(max_digits=4, decimal_places=1)),
            ('family', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ScreenSizeFamily'])),
        ))
        db.send_create_signal('cotizador', ['ScreenSize'])

        # Adding model 'ScreenAspectRatio'
        db.create_table('cotizador_screenaspectratio', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('h_value', self.gf('django.db.models.fields.IntegerField')()),
            ('v_value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['ScreenAspectRatio'])

        # Adding model 'ScreenResolution'
        db.create_table('cotizador_screenresolution', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('h_value', self.gf('django.db.models.fields.IntegerField')()),
            ('v_value', self.gf('django.db.models.fields.IntegerField')()),
            ('total_pixels', self.gf('django.db.models.fields.IntegerField')()),
            ('aspect_ratio', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ScreenAspectRatio'])),
            ('commercial_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['ScreenResolution'])

        # Adding model 'ScreenVideoPort'
        db.create_table('cotizador_screenvideoport', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('port', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceVideoPort'])),
        ))
        db.send_create_signal('cotizador', ['ScreenVideoPort'])

        # Adding model 'ScreenHasVideoPort'
        db.create_table('cotizador_screenhasvideoport', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('quantity', self.gf('django.db.models.fields.IntegerField')()),
            ('port', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ScreenVideoPort'])),
        ))
        db.send_create_signal('cotizador', ['ScreenHasVideoPort'])

        # Adding model 'ScreenPanelType'
        db.create_table('cotizador_screenpaneltype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['ScreenPanelType'])

        # Adding model 'ScreenSpeakers'
        db.create_table('cotizador_screenspeakers', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['ScreenSpeakers'])

        # Adding model 'ScreenResponseTime'
        db.create_table('cotizador_screenresponsetime', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['ScreenResponseTime'])

        # Adding model 'ScreenRefreshRate'
        db.create_table('cotizador_screenrefreshrate', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['ScreenRefreshRate'])

        # Adding model 'ScreenDigitalTuner'
        db.create_table('cotizador_screendigitaltuner', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ordering', self.gf('django.db.models.fields.IntegerField')()),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['ScreenDigitalTuner'])

        # Adding model 'Screen'
        db.create_table('cotizador_screen', (
            ('product_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cotizador.Product'], unique=True, primary_key=True)),
            ('brightness', self.gf('django.db.models.fields.IntegerField')()),
            ('contrast', self.gf('django.db.models.fields.IntegerField')()),
            ('consumption', self.gf('django.db.models.fields.IntegerField')()),
            ('usb_ports', self.gf('django.db.models.fields.IntegerField')()),
            ('has_analog_tuner', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_3d', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('digital_tuner', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ScreenDigitalTuner'])),
            ('response_time', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ScreenResponseTime'])),
            ('refresh_rate', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ScreenRefreshRate'])),
            ('stype', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ScreenType'])),
            ('line', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ScreenLine'])),
            ('display', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ScreenDisplay'])),
            ('size', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ScreenSize'])),
            ('resolution', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ScreenResolution'])),
            ('panel_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ScreenPanelType'])),
            ('speakers', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ScreenSpeakers'])),
        ))
        db.send_create_signal('cotizador', ['Screen'])

        # Adding M2M table for field video_ports on 'Screen'
        db.create_table('cotizador_screen_video_ports', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('screen', models.ForeignKey(orm['cotizador.screen'], null=False)),
            ('screenhasvideoport', models.ForeignKey(orm['cotizador.screenhasvideoport'], null=False))
        ))
        db.create_unique('cotizador_screen_video_ports', ['screen_id', 'screenhasvideoport_id'])

        # Adding model 'MotherboardBrand'
        db.create_table('cotizador_motherboardbrand', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceBrand'])),
        ))
        db.send_create_signal('cotizador', ['MotherboardBrand'])

        # Adding model 'MotherboardGraphics'
        db.create_table('cotizador_motherboardgraphics', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['MotherboardGraphics'])

        # Adding model 'MotherboardSocket'
        db.create_table('cotizador_motherboardsocket', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('socket', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceSocket'])),
        ))
        db.send_create_signal('cotizador', ['MotherboardSocket'])

        # Adding model 'MotherboardSouthbridge'
        db.create_table('cotizador_motherboardsouthbridge', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['MotherboardSouthbridge'])

        # Adding model 'MotherboardChipsetBrand'
        db.create_table('cotizador_motherboardchipsetbrand', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceBrand'])),
        ))
        db.send_create_signal('cotizador', ['MotherboardChipsetBrand'])

        # Adding model 'MotherboardNorthbridgeFamily'
        db.create_table('cotizador_motherboardnorthbridgefamily', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.MotherboardChipsetBrand'])),
            ('socket', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.MotherboardSocket'])),
        ))
        db.send_create_signal('cotizador', ['MotherboardNorthbridgeFamily'])

        # Adding model 'MotherboardNorthbridge'
        db.create_table('cotizador_motherboardnorthbridge', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('family', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.MotherboardNorthbridgeFamily'])),
            ('graphics', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.MotherboardGraphics'])),
        ))
        db.send_create_signal('cotizador', ['MotherboardNorthbridge'])

        # Adding model 'MotherboardChipset'
        db.create_table('cotizador_motherboardchipset', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('northbridge', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.MotherboardNorthbridge'])),
            ('southbridge', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.MotherboardSouthbridge'])),
        ))
        db.send_create_signal('cotizador', ['MotherboardChipset'])

        # Adding model 'MotherboardPort'
        db.create_table('cotizador_motherboardport', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('port', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfacePort'])),
        ))
        db.send_create_signal('cotizador', ['MotherboardPort'])

        # Adding model 'MotherboardHasPort'
        db.create_table('cotizador_motherboardhasport', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('quantity', self.gf('django.db.models.fields.IntegerField')()),
            ('port', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.MotherboardPort'])),
        ))
        db.send_create_signal('cotizador', ['MotherboardHasPort'])

        # Adding model 'MotherboardVideoPort'
        db.create_table('cotizador_motherboardvideoport', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('port', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceVideoPort'])),
        ))
        db.send_create_signal('cotizador', ['MotherboardVideoPort'])

        # Adding model 'MotherboardHasVideoPort'
        db.create_table('cotizador_motherboardhasvideoport', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('quantity', self.gf('django.db.models.fields.IntegerField')()),
            ('port', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.MotherboardVideoPort'])),
        ))
        db.send_create_signal('cotizador', ['MotherboardHasVideoPort'])

        # Adding model 'MotherboardFormat'
        db.create_table('cotizador_motherboardformat', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('format', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceMotherboardFormat'])),
        ))
        db.send_create_signal('cotizador', ['MotherboardFormat'])

        # Adding model 'MotherboardMemoryType'
        db.create_table('cotizador_motherboardmemorytype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('itype', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceMemoryBus'], null=True, blank=True)),
        ))
        db.send_create_signal('cotizador', ['MotherboardMemoryType'])

        # Adding model 'MotherboardHasMemoryType'
        db.create_table('cotizador_motherboardhasmemorytype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('quantity', self.gf('django.db.models.fields.IntegerField')()),
            ('mtype', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.MotherboardMemoryType'])),
        ))
        db.send_create_signal('cotizador', ['MotherboardHasMemoryType'])

        # Adding model 'MotherboardCardBus'
        db.create_table('cotizador_motherboardcardbus', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('bus', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceCardBus'])),
        ))
        db.send_create_signal('cotizador', ['MotherboardCardBus'])

        # Adding model 'MotherboardHasCardBus'
        db.create_table('cotizador_motherboardhascardbus', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('quantity', self.gf('django.db.models.fields.IntegerField')()),
            ('bus', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.MotherboardCardBus'])),
        ))
        db.send_create_signal('cotizador', ['MotherboardHasCardBus'])

        # Adding model 'MotherboardBus'
        db.create_table('cotizador_motherboardbus', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('bus', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceBus'])),
        ))
        db.send_create_signal('cotizador', ['MotherboardBus'])

        # Adding model 'MotherboardHasBus'
        db.create_table('cotizador_motherboardhasbus', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('quantity', self.gf('django.db.models.fields.IntegerField')()),
            ('bus', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.MotherboardBus'])),
        ))
        db.send_create_signal('cotizador', ['MotherboardHasBus'])

        # Adding model 'MotherboardPowerConnector'
        db.create_table('cotizador_motherboardpowerconnector', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('connector', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfacePowerConnector'])),
        ))
        db.send_create_signal('cotizador', ['MotherboardPowerConnector'])

        # Adding model 'MotherboardHasPowerConnector'
        db.create_table('cotizador_motherboardhaspowerconnector', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('quantity', self.gf('django.db.models.fields.IntegerField')()),
            ('connector', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.MotherboardPowerConnector'])),
        ))
        db.send_create_signal('cotizador', ['MotherboardHasPowerConnector'])

        # Adding model 'MotherboardMemoryChannel'
        db.create_table('cotizador_motherboardmemorychannel', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['MotherboardMemoryChannel'])

        # Adding model 'MotherboardAudioChannels'
        db.create_table('cotizador_motherboardaudiochannels', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['MotherboardAudioChannels'])

        # Adding model 'Motherboard'
        db.create_table('cotizador_motherboard', (
            ('product_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cotizador.Product'], unique=True, primary_key=True)),
            ('allows_raid', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('allows_sli', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('allows_cf', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.MotherboardBrand'])),
            ('chipset', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.MotherboardChipset'])),
            ('format', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.MotherboardFormat'])),
            ('memory_channels', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.MotherboardMemoryChannel'])),
            ('audio_channels', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.MotherboardAudioChannels'])),
        ))
        db.send_create_signal('cotizador', ['Motherboard'])

        # Adding M2M table for field ports on 'Motherboard'
        db.create_table('cotizador_motherboard_ports', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('motherboard', models.ForeignKey(orm['cotizador.motherboard'], null=False)),
            ('motherboardhasport', models.ForeignKey(orm['cotizador.motherboardhasport'], null=False))
        ))
        db.create_unique('cotizador_motherboard_ports', ['motherboard_id', 'motherboardhasport_id'])

        # Adding M2M table for field video_ports on 'Motherboard'
        db.create_table('cotizador_motherboard_video_ports', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('motherboard', models.ForeignKey(orm['cotizador.motherboard'], null=False)),
            ('motherboardhasvideoport', models.ForeignKey(orm['cotizador.motherboardhasvideoport'], null=False))
        ))
        db.create_unique('cotizador_motherboard_video_ports', ['motherboard_id', 'motherboardhasvideoport_id'])

        # Adding M2M table for field memory_types on 'Motherboard'
        db.create_table('cotizador_motherboard_memory_types', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('motherboard', models.ForeignKey(orm['cotizador.motherboard'], null=False)),
            ('motherboardhasmemorytype', models.ForeignKey(orm['cotizador.motherboardhasmemorytype'], null=False))
        ))
        db.create_unique('cotizador_motherboard_memory_types', ['motherboard_id', 'motherboardhasmemorytype_id'])

        # Adding M2M table for field card_buses on 'Motherboard'
        db.create_table('cotizador_motherboard_card_buses', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('motherboard', models.ForeignKey(orm['cotizador.motherboard'], null=False)),
            ('motherboardhascardbus', models.ForeignKey(orm['cotizador.motherboardhascardbus'], null=False))
        ))
        db.create_unique('cotizador_motherboard_card_buses', ['motherboard_id', 'motherboardhascardbus_id'])

        # Adding M2M table for field buses on 'Motherboard'
        db.create_table('cotizador_motherboard_buses', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('motherboard', models.ForeignKey(orm['cotizador.motherboard'], null=False)),
            ('motherboardhasbus', models.ForeignKey(orm['cotizador.motherboardhasbus'], null=False))
        ))
        db.create_unique('cotizador_motherboard_buses', ['motherboard_id', 'motherboardhasbus_id'])

        # Adding M2M table for field power_connectors on 'Motherboard'
        db.create_table('cotizador_motherboard_power_connectors', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('motherboard', models.ForeignKey(orm['cotizador.motherboard'], null=False)),
            ('motherboardhaspowerconnector', models.ForeignKey(orm['cotizador.motherboardhaspowerconnector'], null=False))
        ))
        db.create_unique('cotizador_motherboard_power_connectors', ['motherboard_id', 'motherboardhaspowerconnector_id'])

        # Adding model 'CellCompany'
        db.create_table('cotizador_cellcompany', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('store', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.Store'])),
        ))
        db.send_create_signal('cotizador', ['CellCompany'])

        # Adding model 'CellPricingPlan'
        db.create_table('cotizador_cellpricingplan', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.CellCompany'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('price', self.gf('django.db.models.fields.IntegerField')()),
            ('ordering', self.gf('django.db.models.fields.IntegerField')(default=2)),
            ('includes_data', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('cotizador', ['CellPricingPlan'])

        # Adding model 'CellPricing'
        db.create_table('cotizador_cellpricing', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.CellCompany'])),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('best_tier', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.CellPricingTier'], null=True, blank=True)),
        ))
        db.send_create_signal('cotizador', ['CellPricing'])

        # Adding model 'CellPricingTier'
        db.create_table('cotizador_cellpricingtier', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('pricing', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.CellPricing'])),
            ('plan', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.CellPricingPlan'])),
            ('shpe', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.StoreHasProductEntity'], null=True, blank=True)),
            ('cellphone_price', self.gf('django.db.models.fields.IntegerField')()),
            ('monthly_quota', self.gf('django.db.models.fields.IntegerField')()),
            ('three_month_pricing', self.gf('django.db.models.fields.IntegerField')()),
            ('six_month_pricing', self.gf('django.db.models.fields.IntegerField')()),
            ('twelve_month_pricing', self.gf('django.db.models.fields.IntegerField')()),
            ('ordering_cellphone_price', self.gf('django.db.models.fields.BigIntegerField')(default=0)),
            ('ordering_three_month_price', self.gf('django.db.models.fields.BigIntegerField')(default=0)),
            ('ordering_six_month_price', self.gf('django.db.models.fields.BigIntegerField')(default=0)),
            ('ordering_twelve_month_price', self.gf('django.db.models.fields.BigIntegerField')(default=0)),
        ))
        db.send_create_signal('cotizador', ['CellPricingTier'])

        # Adding model 'CellphoneFormFactor'
        db.create_table('cotizador_cellphoneformfactor', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['CellphoneFormFactor'])

        # Adding model 'CellphoneCategory'
        db.create_table('cotizador_cellphonecategory', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['CellphoneCategory'])

        # Adding model 'CellphoneGraphics'
        db.create_table('cotizador_cellphonegraphics', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['CellphoneGraphics'])

        # Adding model 'CellphoneRam'
        db.create_table('cotizador_cellphoneram', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['CellphoneRam'])

        # Adding model 'CellphoneManufacturer'
        db.create_table('cotizador_cellphonemanufacturer', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceBrand'])),
        ))
        db.send_create_signal('cotizador', ['CellphoneManufacturer'])

        # Adding model 'CellphoneOperatingSystem'
        db.create_table('cotizador_cellphoneoperatingsystem', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['CellphoneOperatingSystem'])

        # Adding model 'CellphoneKeyboard'
        db.create_table('cotizador_cellphonekeyboard', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['CellphoneKeyboard'])

        # Adding model 'CellphoneCamera'
        db.create_table('cotizador_cellphonecamera', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('mp', self.gf('django.db.models.fields.DecimalField')(max_digits=3, decimal_places=1)),
        ))
        db.send_create_signal('cotizador', ['CellphoneCamera'])

        # Adding model 'CellphoneCardReader'
        db.create_table('cotizador_cellphonecardreader', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['CellphoneCardReader'])

        # Adding model 'CellphoneScreenSize'
        db.create_table('cotizador_cellphonescreensize', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.DecimalField')(max_digits=2, decimal_places=1)),
        ))
        db.send_create_signal('cotizador', ['CellphoneScreenSize'])

        # Adding model 'CellphoneScreenResolution'
        db.create_table('cotizador_cellphonescreenresolution', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('width', self.gf('django.db.models.fields.IntegerField')()),
            ('height', self.gf('django.db.models.fields.IntegerField')()),
            ('total_pixels', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['CellphoneScreenResolution'])

        # Adding model 'CellphoneScreenColors'
        db.create_table('cotizador_cellphonescreencolors', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('quantity', self.gf('django.db.models.fields.IntegerField')()),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['CellphoneScreenColors'])

        # Adding model 'CellphoneScreen'
        db.create_table('cotizador_cellphonescreen', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('size', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.CellphoneScreenSize'])),
            ('resolution', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.CellphoneScreenResolution'])),
            ('colors', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.CellphoneScreenColors'])),
            ('is_touch', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('cotizador', ['CellphoneScreen'])

        # Adding model 'CellphoneProcessor'
        db.create_table('cotizador_cellphoneprocessor', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('frequency', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['CellphoneProcessor'])

        # Adding model 'Cellphone'
        db.create_table('cotizador_cellphone', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('weight', self.gf('django.db.models.fields.IntegerField')()),
            ('width', self.gf('django.db.models.fields.IntegerField')()),
            ('height', self.gf('django.db.models.fields.IntegerField')()),
            ('depth', self.gf('django.db.models.fields.IntegerField')()),
            ('internal_memory', self.gf('django.db.models.fields.IntegerField')()),
            ('battery', self.gf('django.db.models.fields.IntegerField')()),
            ('records_video', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('plays_mp3', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('has_bluetooth', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('has_wifi', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('has_3g', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('has_gps', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('has_headphones_output', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('form_factor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.CellphoneFormFactor'])),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.CellphoneCategory'])),
            ('graphics', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.CellphoneGraphics'], null=True, blank=True)),
            ('ram', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.CellphoneRam'])),
            ('manufacturer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.CellphoneManufacturer'])),
            ('operating_system', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.CellphoneOperatingSystem'])),
            ('keyboard', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.CellphoneKeyboard'])),
            ('camera', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.CellphoneCamera'])),
            ('card_reader', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.CellphoneCardReader'])),
            ('screen', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.CellphoneScreen'])),
            ('processor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.CellphoneProcessor'], null=True, blank=True)),
        ))
        db.send_create_signal('cotizador', ['Cellphone'])

        # Adding model 'Cell'
        db.create_table('cotizador_cell', (
            ('product_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cotizador.Product'], unique=True, primary_key=True)),
            ('pricing', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cotizador.CellPricing'], unique=True)),
            ('phone', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.Cellphone'])),
        ))
        db.send_create_signal('cotizador', ['Cell'])

        # Adding model 'RamBrand'
        db.create_table('cotizador_rambrand', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceBrand'])),
        ))
        db.send_create_signal('cotizador', ['RamBrand'])

        # Adding model 'RamLine'
        db.create_table('cotizador_ramline', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.RamBrand'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['RamLine'])

        # Adding model 'RamMemoryBus'
        db.create_table('cotizador_rammemorybus', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('bus', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceMemoryBus'])),
        ))
        db.send_create_signal('cotizador', ['RamMemoryBus'])

        # Adding model 'RamFrequency'
        db.create_table('cotizador_ramfrequency', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['RamFrequency'])

        # Adding model 'RamBus'
        db.create_table('cotizador_rambus', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('bus', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.RamMemoryBus'])),
            ('frequency', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.RamFrequency'])),
        ))
        db.send_create_signal('cotizador', ['RamBus'])

        # Adding model 'RamDimmCapacity'
        db.create_table('cotizador_ramdimmcapacity', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['RamDimmCapacity'])

        # Adding model 'RamDimmQuantity'
        db.create_table('cotizador_ramdimmquantity', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['RamDimmQuantity'])

        # Adding model 'RamTotalCapacity'
        db.create_table('cotizador_ramtotalcapacity', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['RamTotalCapacity'])

        # Adding model 'RamCapacity'
        db.create_table('cotizador_ramcapacity', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('dimm_quantity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.RamDimmQuantity'])),
            ('dimm_capacity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.RamDimmCapacity'])),
            ('total_capacity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.RamTotalCapacity'])),
        ))
        db.send_create_signal('cotizador', ['RamCapacity'])

        # Adding model 'RamVoltage'
        db.create_table('cotizador_ramvoltage', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.DecimalField')(max_digits=4, decimal_places=2)),
        ))
        db.send_create_signal('cotizador', ['RamVoltage'])

        # Adding model 'RamLatencyCl'
        db.create_table('cotizador_ramlatencycl', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['RamLatencyCl'])

        # Adding model 'RamLatencyTras'
        db.create_table('cotizador_ramlatencytras', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['RamLatencyTras'])

        # Adding model 'RamLatencyTrcd'
        db.create_table('cotizador_ramlatencytrcd', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['RamLatencyTrcd'])

        # Adding model 'RamLatencyTrp'
        db.create_table('cotizador_ramlatencytrp', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['RamLatencyTrp'])

        # Adding model 'Ram'
        db.create_table('cotizador_ram', (
            ('product_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cotizador.Product'], unique=True, primary_key=True)),
            ('line', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.RamLine'])),
            ('capacity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.RamCapacity'])),
            ('bus', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.RamBus'])),
            ('voltage', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.RamVoltage'])),
            ('latency_cl', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.RamLatencyCl'], null=True, blank=True)),
            ('latency_trcd', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.RamLatencyTrcd'], null=True, blank=True)),
            ('latency_trp', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.RamLatencyTrp'], null=True, blank=True)),
            ('latency_tras', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.RamLatencyTras'], null=True, blank=True)),
            ('is_ecc', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_fully_buffered', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('cotizador', ['Ram'])

        # Adding model 'StorageDriveBrand'
        db.create_table('cotizador_storagedrivebrand', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceBrand'])),
        ))
        db.send_create_signal('cotizador', ['StorageDriveBrand'])

        # Adding model 'StorageDriveBuffer'
        db.create_table('cotizador_storagedrivebuffer', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['StorageDriveBuffer'])

        # Adding model 'StorageDriveBus'
        db.create_table('cotizador_storagedrivebus', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('bus', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceBus'])),
        ))
        db.send_create_signal('cotizador', ['StorageDriveBus'])

        # Adding model 'StorageDriveCapacity'
        db.create_table('cotizador_storagedrivecapacity', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['StorageDriveCapacity'])

        # Adding model 'StorageDriveFamily'
        db.create_table('cotizador_storagedrivefamily', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.StorageDriveBrand'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['StorageDriveFamily'])

        # Adding model 'StorageDriveLine'
        db.create_table('cotizador_storagedriveline', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('family', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.StorageDriveFamily'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['StorageDriveLine'])

        # Adding model 'StorageDriveRpm'
        db.create_table('cotizador_storagedriverpm', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['StorageDriveRpm'])

        # Adding model 'StorageDriveSize'
        db.create_table('cotizador_storagedrivesize', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.DecimalField')(max_digits=3, decimal_places=1)),
        ))
        db.send_create_signal('cotizador', ['StorageDriveSize'])

        # Adding model 'StorageDriveType'
        db.create_table('cotizador_storagedrivetype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['StorageDriveType'])

        # Adding model 'StorageDrive'
        db.create_table('cotizador_storagedrive', (
            ('product_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cotizador.Product'], unique=True, primary_key=True)),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.StorageDriveType'])),
            ('line', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.StorageDriveLine'])),
            ('capacity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.StorageDriveCapacity'])),
            ('rpm', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.StorageDriveRpm'])),
            ('size', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.StorageDriveSize'])),
            ('bus', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.StorageDriveBus'])),
            ('buffer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.StorageDriveBuffer'])),
            ('sequential_read_speed', self.gf('django.db.models.fields.IntegerField')()),
            ('sequential_write_speed', self.gf('django.db.models.fields.IntegerField')()),
            ('random_read_speed', self.gf('django.db.models.fields.IntegerField')()),
            ('random_write_speed', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['StorageDrive'])

        # Adding model 'PowerSupplyBrand'
        db.create_table('cotizador_powersupplybrand', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceBrand'])),
        ))
        db.send_create_signal('cotizador', ['PowerSupplyBrand'])

        # Adding model 'PowerSupplyLine'
        db.create_table('cotizador_powersupplyline', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.PowerSupplyBrand'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['PowerSupplyLine'])

        # Adding model 'PowerSupplyPower'
        db.create_table('cotizador_powersupplypower', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['PowerSupplyPower'])

        # Adding model 'PowerSupplyCertification'
        db.create_table('cotizador_powersupplycertification', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('value', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['PowerSupplyCertification'])

        # Adding model 'PowerSupplySize'
        db.create_table('cotizador_powersupplysize', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['PowerSupplySize'])

        # Adding model 'PowerSupplyPowerConnector'
        db.create_table('cotizador_powersupplypowerconnector', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('connector', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfacePowerConnector'])),
        ))
        db.send_create_signal('cotizador', ['PowerSupplyPowerConnector'])

        # Adding model 'PowerSupplyHasPowerConnector'
        db.create_table('cotizador_powersupplyhaspowerconnector', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('quantity', self.gf('django.db.models.fields.IntegerField')()),
            ('connector', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.PowerSupplyPowerConnector'])),
        ))
        db.send_create_signal('cotizador', ['PowerSupplyHasPowerConnector'])

        # Adding model 'PowerSupply'
        db.create_table('cotizador_powersupply', (
            ('product_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cotizador.Product'], unique=True, primary_key=True)),
            ('line', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.PowerSupplyLine'])),
            ('power', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.PowerSupplyPower'])),
            ('certification', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.PowerSupplyCertification'])),
            ('size', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.PowerSupplySize'])),
            ('is_modular', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('has_active_pfc', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('currents_on_12V_rails', self.gf('django.db.models.fields.CommaSeparatedIntegerField')(max_length=255)),
            ('currents_on_5V_rails', self.gf('django.db.models.fields.CommaSeparatedIntegerField')(max_length=255)),
            ('currents_on_33V_rails', self.gf('django.db.models.fields.CommaSeparatedIntegerField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['PowerSupply'])

        # Adding M2M table for field power_connectors on 'PowerSupply'
        db.create_table('cotizador_powersupply_power_connectors', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('powersupply', models.ForeignKey(orm['cotizador.powersupply'], null=False)),
            ('powersupplyhaspowerconnector', models.ForeignKey(orm['cotizador.powersupplyhaspowerconnector'], null=False))
        ))
        db.create_unique('cotizador_powersupply_power_connectors', ['powersupply_id', 'powersupplyhaspowerconnector_id'])

        # Adding model 'ComputerCaseFan'
        db.create_table('cotizador_computercasefan', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('mm', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['ComputerCaseFan'])

        # Adding model 'ComputerCaseBrand'
        db.create_table('cotizador_computercasebrand', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceBrand'])),
        ))
        db.send_create_signal('cotizador', ['ComputerCaseBrand'])

        # Adding model 'ComputerCaseFanDistribution'
        db.create_table('cotizador_computercasefandistribution', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('quantity', self.gf('django.db.models.fields.IntegerField')()),
            ('fan', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ComputerCaseFan'])),
        ))
        db.send_create_signal('cotizador', ['ComputerCaseFanDistribution'])

        # Adding model 'ComputerCaseMotherboardFormat'
        db.create_table('cotizador_computercasemotherboardformat', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('format', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.InterfaceMotherboardFormat'])),
        ))
        db.send_create_signal('cotizador', ['ComputerCaseMotherboardFormat'])

        # Adding model 'ComputerCasePowerSupply'
        db.create_table('cotizador_computercasepowersupply', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('power', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('cotizador', ['ComputerCasePowerSupply'])

        # Adding model 'ComputerCasePowerSupplyPosition'
        db.create_table('cotizador_computercasepowersupplyposition', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('cotizador', ['ComputerCasePowerSupplyPosition'])

        # Adding model 'ComputerCase'
        db.create_table('cotizador_computercase', (
            ('product_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cotizador.Product'], unique=True, primary_key=True)),
            ('external_5_1_4_bays', self.gf('django.db.models.fields.IntegerField')()),
            ('internal_3_1_2_bays', self.gf('django.db.models.fields.IntegerField')()),
            ('external_3_1_2_bays', self.gf('django.db.models.fields.IntegerField')()),
            ('internal_2_1_2_bays', self.gf('django.db.models.fields.IntegerField')()),
            ('rear_expansion_slots', self.gf('django.db.models.fields.IntegerField')()),
            ('front_usb_ports', self.gf('django.db.models.fields.IntegerField')()),
            ('length', self.gf('django.db.models.fields.IntegerField')()),
            ('height', self.gf('django.db.models.fields.IntegerField')()),
            ('width', self.gf('django.db.models.fields.IntegerField')()),
            ('weight', self.gf('django.db.models.fields.IntegerField')()),
            ('has_front_audio_ports', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('has_front_esata_port', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('has_front_firewire_port', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('has_motherboard_tray', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('brand', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ComputerCaseBrand'])),
            ('largest_motherboard_format', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ComputerCaseMotherboardFormat'])),
            ('power_supply', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ComputerCasePowerSupply'])),
            ('power_supply_position', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cotizador.ComputerCasePowerSupplyPosition'])),
        ))
        db.send_create_signal('cotizador', ['ComputerCase'])

        # Adding M2M table for field frontal_fan_slots on 'ComputerCase'
        db.create_table('cotizador_computercase_frontal_fan_slots', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('computercase', models.ForeignKey(orm['cotizador.computercase'], null=False)),
            ('computercasefandistribution', models.ForeignKey(orm['cotizador.computercasefandistribution'], null=False))
        ))
        db.create_unique('cotizador_computercase_frontal_fan_slots', ['computercase_id', 'computercasefandistribution_id'])

        # Adding M2M table for field rear_fan_slots on 'ComputerCase'
        db.create_table('cotizador_computercase_rear_fan_slots', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('computercase', models.ForeignKey(orm['cotizador.computercase'], null=False)),
            ('computercasefandistribution', models.ForeignKey(orm['cotizador.computercasefandistribution'], null=False))
        ))
        db.create_unique('cotizador_computercase_rear_fan_slots', ['computercase_id', 'computercasefandistribution_id'])

        # Adding M2M table for field side_fan_slots on 'ComputerCase'
        db.create_table('cotizador_computercase_side_fan_slots', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('computercase', models.ForeignKey(orm['cotizador.computercase'], null=False)),
            ('computercasefandistribution', models.ForeignKey(orm['cotizador.computercasefandistribution'], null=False))
        ))
        db.create_unique('cotizador_computercase_side_fan_slots', ['computercase_id', 'computercasefandistribution_id'])

        # Adding M2M table for field top_fan_slots on 'ComputerCase'
        db.create_table('cotizador_computercase_top_fan_slots', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('computercase', models.ForeignKey(orm['cotizador.computercase'], null=False)),
            ('computercasefandistribution', models.ForeignKey(orm['cotizador.computercasefandistribution'], null=False))
        ))
        db.create_unique('cotizador_computercase_top_fan_slots', ['computercase_id', 'computercasefandistribution_id'])

        # Adding M2M table for field bottom_fan_slots on 'ComputerCase'
        db.create_table('cotizador_computercase_bottom_fan_slots', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('computercase', models.ForeignKey(orm['cotizador.computercase'], null=False)),
            ('computercasefandistribution', models.ForeignKey(orm['cotizador.computercasefandistribution'], null=False))
        ))
        db.create_unique('cotizador_computercase_bottom_fan_slots', ['computercase_id', 'computercasefandistribution_id'])

        # Adding M2M table for field included_fans on 'ComputerCase'
        db.create_table('cotizador_computercase_included_fans', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('computercase', models.ForeignKey(orm['cotizador.computercase'], null=False)),
            ('computercasefandistribution', models.ForeignKey(orm['cotizador.computercasefandistribution'], null=False))
        ))
        db.create_unique('cotizador_computercase_included_fans', ['computercase_id', 'computercasefandistribution_id'])


    def backwards(self, orm):
        
        # Removing unique constraint on 'AdvertisementImpressionPerDay', fields ['advertisement', 'date']
        db.delete_unique('cotizador_advertisementimpressionperday', ['advertisement_id', 'date'])

        # Deleting model 'InterfaceBrand'
        db.delete_table('cotizador_interfacebrand')

        # Deleting model 'InterfaceVideoPort'
        db.delete_table('cotizador_interfacevideoport')

        # Deleting model 'InterfaceCardBusName'
        db.delete_table('cotizador_interfacecardbusname')

        # Deleting model 'InterfaceCardBusLane'
        db.delete_table('cotizador_interfacecardbuslane')

        # Deleting model 'InterfaceCardBus'
        db.delete_table('cotizador_interfacecardbus')

        # Deleting model 'InterfaceSocketBrand'
        db.delete_table('cotizador_interfacesocketbrand')

        # Deleting model 'InterfaceSocket'
        db.delete_table('cotizador_interfacesocket')

        # Deleting model 'InterfacePort'
        db.delete_table('cotizador_interfaceport')

        # Deleting model 'InterfaceMemoryFormat'
        db.delete_table('cotizador_interfacememoryformat')

        # Deleting model 'InterfaceMemoryType'
        db.delete_table('cotizador_interfacememorytype')

        # Deleting model 'InterfaceMemoryBus'
        db.delete_table('cotizador_interfacememorybus')

        # Deleting model 'InterfaceBus'
        db.delete_table('cotizador_interfacebus')

        # Deleting model 'InterfacePowerConnector'
        db.delete_table('cotizador_interfacepowerconnector')

        # Deleting model 'InterfaceMotherboardFormat'
        db.delete_table('cotizador_interfacemotherboardformat')

        # Deleting model 'ProductType'
        db.delete_table('cotizador_producttype')

        # Deleting model 'NotebookProcessorBrand'
        db.delete_table('cotizador_notebookprocessorbrand')

        # Deleting model 'NotebookProcessorLineFamily'
        db.delete_table('cotizador_notebookprocessorlinefamily')

        # Deleting model 'NotebookProcessorLine'
        db.delete_table('cotizador_notebookprocessorline')

        # Deleting model 'NotebookProcessorFrequency'
        db.delete_table('cotizador_notebookprocessorfrequency')

        # Deleting model 'NotebookProcessorCache'
        db.delete_table('cotizador_notebookprocessorcache')

        # Deleting model 'NotebookProcessorFSB'
        db.delete_table('cotizador_notebookprocessorfsb')

        # Deleting model 'NotebookProcessorMultiplier'
        db.delete_table('cotizador_notebookprocessormultiplier')

        # Deleting model 'NotebookProcessorSocket'
        db.delete_table('cotizador_notebookprocessorsocket')

        # Deleting model 'NotebookProcessorManufacturing'
        db.delete_table('cotizador_notebookprocessormanufacturing')

        # Deleting model 'NotebookProcessorFamily'
        db.delete_table('cotizador_notebookprocessorfamily')

        # Deleting model 'NotebookProcessor'
        db.delete_table('cotizador_notebookprocessor')

        # Deleting model 'NotebookOpticalDrive'
        db.delete_table('cotizador_notebookopticaldrive')

        # Deleting model 'NotebookBrand'
        db.delete_table('cotizador_notebookbrand')

        # Deleting model 'NotebookLine'
        db.delete_table('cotizador_notebookline')

        # Deleting model 'NotebookLan'
        db.delete_table('cotizador_notebooklan')

        # Deleting model 'NotebookOperatingSystemBrand'
        db.delete_table('cotizador_notebookoperatingsystembrand')

        # Deleting model 'NotebookOperatingSystemFamily'
        db.delete_table('cotizador_notebookoperatingsystemfamily')

        # Deleting model 'NotebookOperatingSystemLanguage'
        db.delete_table('cotizador_notebookoperatingsystemlanguage')

        # Deleting model 'NotebookOperatingSystem'
        db.delete_table('cotizador_notebookoperatingsystem')

        # Deleting model 'NotebookVideoCardMemory'
        db.delete_table('cotizador_notebookvideocardmemory')

        # Deleting model 'NotebookVideoCardType'
        db.delete_table('cotizador_notebookvideocardtype')

        # Deleting model 'NotebookVideoCardBrand'
        db.delete_table('cotizador_notebookvideocardbrand')

        # Deleting model 'NotebookVideoCardLine'
        db.delete_table('cotizador_notebookvideocardline')

        # Deleting model 'NotebookVideoCard'
        db.delete_table('cotizador_notebookvideocard')

        # Deleting model 'NotebookWifiCardBrand'
        db.delete_table('cotizador_notebookwificardbrand')

        # Deleting model 'NotebookWifiCardNorm'
        db.delete_table('cotizador_notebookwificardnorm')

        # Deleting model 'NotebookWifiCard'
        db.delete_table('cotizador_notebookwificard')

        # Deleting model 'NotebookVideoPort'
        db.delete_table('cotizador_notebookvideoport')

        # Deleting model 'NotebookScreenResolution'
        db.delete_table('cotizador_notebookscreenresolution')

        # Deleting model 'NotebookScreenSizeFamily'
        db.delete_table('cotizador_notebookscreensizefamily')

        # Deleting model 'NotebookScreenSize'
        db.delete_table('cotizador_notebookscreensize')

        # Deleting model 'NotebookScreen'
        db.delete_table('cotizador_notebookscreen')

        # Deleting model 'NotebookPowerAdapter'
        db.delete_table('cotizador_notebookpoweradapter')

        # Deleting model 'NotebookStorageDriveType'
        db.delete_table('cotizador_notebookstoragedrivetype')

        # Deleting model 'NotebookStorageDriveCapacity'
        db.delete_table('cotizador_notebookstoragedrivecapacity')

        # Deleting model 'NotebookStorageDriveRpm'
        db.delete_table('cotizador_notebookstoragedriverpm')

        # Deleting model 'NotebookStorageDrive'
        db.delete_table('cotizador_notebookstoragedrive')

        # Deleting model 'NotebookChipsetBrand'
        db.delete_table('cotizador_notebookchipsetbrand')

        # Deleting model 'NotebookChipset'
        db.delete_table('cotizador_notebookchipset')

        # Deleting model 'NotebookRamQuantity'
        db.delete_table('cotizador_notebookramquantity')

        # Deleting model 'NotebookRamType'
        db.delete_table('cotizador_notebookramtype')

        # Deleting model 'NotebookRamFrequency'
        db.delete_table('cotizador_notebookramfrequency')

        # Deleting model 'NotebookCardReader'
        db.delete_table('cotizador_notebookcardreader')

        # Deleting model 'Store'
        db.delete_table('cotizador_store')

        # Deleting model 'NotebookType'
        db.delete_table('cotizador_notebooktype')

        # Deleting model 'Product'
        db.delete_table('cotizador_product')

        # Deleting model 'Notebook'
        db.delete_table('cotizador_notebook')

        # Removing M2M table for field video_card on 'Notebook'
        db.delete_table('cotizador_notebook_video_card')

        # Removing M2M table for field video_port on 'Notebook'
        db.delete_table('cotizador_notebook_video_port')

        # Removing M2M table for field storage_drive on 'Notebook'
        db.delete_table('cotizador_notebook_storage_drive')

        # Deleting model 'StoreHasProduct'
        db.delete_table('cotizador_storehasproduct')

        # Deleting model 'StoreHasProductEntity'
        db.delete_table('cotizador_storehasproductentity')

        # Deleting model 'ProductPriceChange'
        db.delete_table('cotizador_productpricechange')

        # Deleting model 'ProductComment'
        db.delete_table('cotizador_productcomment')

        # Deleting model 'ProductPicture'
        db.delete_table('cotizador_productpicture')

        # Deleting model 'StoreProductHistory'
        db.delete_table('cotizador_storeproducthistory')

        # Deleting model 'LogEntry'
        db.delete_table('cotizador_logentry')

        # Deleting model 'LogChangeEntityName'
        db.delete_table('cotizador_logchangeentityname')

        # Deleting model 'LogEntryMessage'
        db.delete_table('cotizador_logentrymessage')

        # Deleting model 'ExternalVisit'
        db.delete_table('cotizador_externalvisit')

        # Deleting model 'SponsoredVisit'
        db.delete_table('cotizador_sponsoredvisit')

        # Deleting model 'AdvertisementPosition'
        db.delete_table('cotizador_advertisementposition')

        # Deleting model 'Advertisement'
        db.delete_table('cotizador_advertisement')

        # Deleting model 'AdvertisementImpression'
        db.delete_table('cotizador_advertisementimpression')

        # Deleting model 'AdvertisementImpressionPerDay'
        db.delete_table('cotizador_advertisementimpressionperday')

        # Deleting model 'AdvertisementVisit'
        db.delete_table('cotizador_advertisementvisit')

        # Deleting model 'SearchRegistry'
        db.delete_table('cotizador_searchregistry')

        # Deleting model 'ProductSubscription'
        db.delete_table('cotizador_productsubscription')

        # Deleting model 'UserProfile'
        db.delete_table('cotizador_userprofile')

        # Removing M2M table for field managed_product_types on 'UserProfile'
        db.delete_table('cotizador_userprofile_managed_product_types')

        # Deleting model 'LogFetchStoreError'
        db.delete_table('cotizador_logfetchstoreerror')

        # Deleting model 'LogNewEntity'
        db.delete_table('cotizador_lognewentity')

        # Deleting model 'LogReviveEntity'
        db.delete_table('cotizador_logreviveentity')

        # Deleting model 'LogLostEntity'
        db.delete_table('cotizador_loglostentity')

        # Deleting model 'LogChangeEntityPrice'
        db.delete_table('cotizador_logchangeentityprice')

        # Deleting model 'LogReviveProduct'
        db.delete_table('cotizador_logreviveproduct')

        # Deleting model 'LogLostProduct'
        db.delete_table('cotizador_loglostproduct')

        # Deleting model 'LogChangeProductPrice'
        db.delete_table('cotizador_logchangeproductprice')

        # Deleting model 'MailChangeProductPrice'
        db.delete_table('cotizador_mailchangeproductprice')

        # Deleting model 'MailLostProduct'
        db.delete_table('cotizador_maillostproduct')

        # Deleting model 'MailReviveProduct'
        db.delete_table('cotizador_mailreviveproduct')

        # Deleting model 'ProductComparisonList'
        db.delete_table('cotizador_productcomparisonlist')

        # Removing M2M table for field products on 'ProductComparisonList'
        db.delete_table('cotizador_productcomparisonlist_products')

        # Deleting model 'ProductVisit'
        db.delete_table('cotizador_productvisit')

        # Deleting model 'StoreCustomUpdateRegistry'
        db.delete_table('cotizador_storecustomupdateregistry')

        # Deleting model 'VideoCardBrand'
        db.delete_table('cotizador_videocardbrand')

        # Deleting model 'VideoCardGpuBrand'
        db.delete_table('cotizador_videocardgpubrand')

        # Deleting model 'VideoCardGpuFamily'
        db.delete_table('cotizador_videocardgpufamily')

        # Deleting model 'VideoCardGpuLine'
        db.delete_table('cotizador_videocardgpuline')

        # Deleting model 'VideoCardGpuArchitecture'
        db.delete_table('cotizador_videocardgpuarchitecture')

        # Deleting model 'VideoCardGpuCoreFamily'
        db.delete_table('cotizador_videocardgpucorefamily')

        # Deleting model 'VideoCardGpuCore'
        db.delete_table('cotizador_videocardgpucore')

        # Deleting model 'VideoCardPort'
        db.delete_table('cotizador_videocardport')

        # Deleting model 'VideoCardHasPort'
        db.delete_table('cotizador_videocardhasport')

        # Deleting model 'VideoCardBus'
        db.delete_table('cotizador_videocardbus')

        # Deleting model 'VideoCardGpuDirectxVersion'
        db.delete_table('cotizador_videocardgpudirectxversion')

        # Deleting model 'VideoCardGpuOpenglVersion'
        db.delete_table('cotizador_videocardgpuopenglversion')

        # Deleting model 'VideoCardRefrigeration'
        db.delete_table('cotizador_videocardrefrigeration')

        # Deleting model 'VideoCardSlotType'
        db.delete_table('cotizador_videocardslottype')

        # Deleting model 'VideoCardProfile'
        db.delete_table('cotizador_videocardprofile')

        # Deleting model 'VideoCardMemoryType'
        db.delete_table('cotizador_videocardmemorytype')

        # Deleting model 'VideoCardMemoryQuantity'
        db.delete_table('cotizador_videocardmemoryquantity')

        # Deleting model 'VideoCardMemoryBusWidth'
        db.delete_table('cotizador_videocardmemorybuswidth')

        # Deleting model 'VideoCardGpuCoreCount'
        db.delete_table('cotizador_videocardgpucorecount')

        # Deleting model 'VideoCardPowerConnector'
        db.delete_table('cotizador_videocardpowerconnector')

        # Deleting model 'VideoCardHasPowerConnector'
        db.delete_table('cotizador_videocardhaspowerconnector')

        # Deleting model 'VideoCardGpuManufacturingProcess'
        db.delete_table('cotizador_videocardgpumanufacturingprocess')

        # Deleting model 'VideoCardGpu'
        db.delete_table('cotizador_videocardgpu')

        # Removing M2M table for field power_conns on 'VideoCardGpu'
        db.delete_table('cotizador_videocardgpu_power_conns')

        # Deleting model 'VideoCard'
        db.delete_table('cotizador_videocard')

        # Removing M2M table for field video_ports on 'VideoCard'
        db.delete_table('cotizador_videocard_video_ports')

        # Deleting model 'ProcessorBrand'
        db.delete_table('cotizador_processorbrand')

        # Deleting model 'ProcessorFamily'
        db.delete_table('cotizador_processorfamily')

        # Deleting model 'ProcessorLine'
        db.delete_table('cotizador_processorline')

        # Deleting model 'ProcessorL2CacheQuantity'
        db.delete_table('cotizador_processorl2cachequantity')

        # Deleting model 'ProcessorL3CacheQuantity'
        db.delete_table('cotizador_processorl3cachequantity')

        # Deleting model 'ProcessorL2Cache'
        db.delete_table('cotizador_processorl2cache')

        # Deleting model 'ProcessorL3Cache'
        db.delete_table('cotizador_processorl3cache')

        # Deleting model 'ProcessorSocket'
        db.delete_table('cotizador_processorsocket')

        # Deleting model 'ProcessorCoreCount'
        db.delete_table('cotizador_processorcorecount')

        # Deleting model 'ProcessorArchitecture'
        db.delete_table('cotizador_processorarchitecture')

        # Deleting model 'ProcessorManufacturingProcess'
        db.delete_table('cotizador_processormanufacturingprocess')

        # Deleting model 'ProcessorCore'
        db.delete_table('cotizador_processorcore')

        # Deleting model 'ProcessorMultiplier'
        db.delete_table('cotizador_processormultiplier')

        # Deleting model 'ProcessorFsb'
        db.delete_table('cotizador_processorfsb')

        # Deleting model 'ProcessorGraphics'
        db.delete_table('cotizador_processorgraphics')

        # Deleting model 'Processor'
        db.delete_table('cotizador_processor')

        # Deleting model 'ScreenType'
        db.delete_table('cotizador_screentype')

        # Deleting model 'ScreenBrand'
        db.delete_table('cotizador_screenbrand')

        # Deleting model 'ScreenLine'
        db.delete_table('cotizador_screenline')

        # Deleting model 'ScreenDisplayType'
        db.delete_table('cotizador_screendisplaytype')

        # Deleting model 'ScreenDisplay'
        db.delete_table('cotizador_screendisplay')

        # Deleting model 'ScreenSizeFamily'
        db.delete_table('cotizador_screensizefamily')

        # Deleting model 'ScreenSize'
        db.delete_table('cotizador_screensize')

        # Deleting model 'ScreenAspectRatio'
        db.delete_table('cotizador_screenaspectratio')

        # Deleting model 'ScreenResolution'
        db.delete_table('cotizador_screenresolution')

        # Deleting model 'ScreenVideoPort'
        db.delete_table('cotizador_screenvideoport')

        # Deleting model 'ScreenHasVideoPort'
        db.delete_table('cotizador_screenhasvideoport')

        # Deleting model 'ScreenPanelType'
        db.delete_table('cotizador_screenpaneltype')

        # Deleting model 'ScreenSpeakers'
        db.delete_table('cotizador_screenspeakers')

        # Deleting model 'ScreenResponseTime'
        db.delete_table('cotizador_screenresponsetime')

        # Deleting model 'ScreenRefreshRate'
        db.delete_table('cotizador_screenrefreshrate')

        # Deleting model 'ScreenDigitalTuner'
        db.delete_table('cotizador_screendigitaltuner')

        # Deleting model 'Screen'
        db.delete_table('cotizador_screen')

        # Removing M2M table for field video_ports on 'Screen'
        db.delete_table('cotizador_screen_video_ports')

        # Deleting model 'MotherboardBrand'
        db.delete_table('cotizador_motherboardbrand')

        # Deleting model 'MotherboardGraphics'
        db.delete_table('cotizador_motherboardgraphics')

        # Deleting model 'MotherboardSocket'
        db.delete_table('cotizador_motherboardsocket')

        # Deleting model 'MotherboardSouthbridge'
        db.delete_table('cotizador_motherboardsouthbridge')

        # Deleting model 'MotherboardChipsetBrand'
        db.delete_table('cotizador_motherboardchipsetbrand')

        # Deleting model 'MotherboardNorthbridgeFamily'
        db.delete_table('cotizador_motherboardnorthbridgefamily')

        # Deleting model 'MotherboardNorthbridge'
        db.delete_table('cotizador_motherboardnorthbridge')

        # Deleting model 'MotherboardChipset'
        db.delete_table('cotizador_motherboardchipset')

        # Deleting model 'MotherboardPort'
        db.delete_table('cotizador_motherboardport')

        # Deleting model 'MotherboardHasPort'
        db.delete_table('cotizador_motherboardhasport')

        # Deleting model 'MotherboardVideoPort'
        db.delete_table('cotizador_motherboardvideoport')

        # Deleting model 'MotherboardHasVideoPort'
        db.delete_table('cotizador_motherboardhasvideoport')

        # Deleting model 'MotherboardFormat'
        db.delete_table('cotizador_motherboardformat')

        # Deleting model 'MotherboardMemoryType'
        db.delete_table('cotizador_motherboardmemorytype')

        # Deleting model 'MotherboardHasMemoryType'
        db.delete_table('cotizador_motherboardhasmemorytype')

        # Deleting model 'MotherboardCardBus'
        db.delete_table('cotizador_motherboardcardbus')

        # Deleting model 'MotherboardHasCardBus'
        db.delete_table('cotizador_motherboardhascardbus')

        # Deleting model 'MotherboardBus'
        db.delete_table('cotizador_motherboardbus')

        # Deleting model 'MotherboardHasBus'
        db.delete_table('cotizador_motherboardhasbus')

        # Deleting model 'MotherboardPowerConnector'
        db.delete_table('cotizador_motherboardpowerconnector')

        # Deleting model 'MotherboardHasPowerConnector'
        db.delete_table('cotizador_motherboardhaspowerconnector')

        # Deleting model 'MotherboardMemoryChannel'
        db.delete_table('cotizador_motherboardmemorychannel')

        # Deleting model 'MotherboardAudioChannels'
        db.delete_table('cotizador_motherboardaudiochannels')

        # Deleting model 'Motherboard'
        db.delete_table('cotizador_motherboard')

        # Removing M2M table for field ports on 'Motherboard'
        db.delete_table('cotizador_motherboard_ports')

        # Removing M2M table for field video_ports on 'Motherboard'
        db.delete_table('cotizador_motherboard_video_ports')

        # Removing M2M table for field memory_types on 'Motherboard'
        db.delete_table('cotizador_motherboard_memory_types')

        # Removing M2M table for field card_buses on 'Motherboard'
        db.delete_table('cotizador_motherboard_card_buses')

        # Removing M2M table for field buses on 'Motherboard'
        db.delete_table('cotizador_motherboard_buses')

        # Removing M2M table for field power_connectors on 'Motherboard'
        db.delete_table('cotizador_motherboard_power_connectors')

        # Deleting model 'CellCompany'
        db.delete_table('cotizador_cellcompany')

        # Deleting model 'CellPricingPlan'
        db.delete_table('cotizador_cellpricingplan')

        # Deleting model 'CellPricing'
        db.delete_table('cotizador_cellpricing')

        # Deleting model 'CellPricingTier'
        db.delete_table('cotizador_cellpricingtier')

        # Deleting model 'CellphoneFormFactor'
        db.delete_table('cotizador_cellphoneformfactor')

        # Deleting model 'CellphoneCategory'
        db.delete_table('cotizador_cellphonecategory')

        # Deleting model 'CellphoneGraphics'
        db.delete_table('cotizador_cellphonegraphics')

        # Deleting model 'CellphoneRam'
        db.delete_table('cotizador_cellphoneram')

        # Deleting model 'CellphoneManufacturer'
        db.delete_table('cotizador_cellphonemanufacturer')

        # Deleting model 'CellphoneOperatingSystem'
        db.delete_table('cotizador_cellphoneoperatingsystem')

        # Deleting model 'CellphoneKeyboard'
        db.delete_table('cotizador_cellphonekeyboard')

        # Deleting model 'CellphoneCamera'
        db.delete_table('cotizador_cellphonecamera')

        # Deleting model 'CellphoneCardReader'
        db.delete_table('cotizador_cellphonecardreader')

        # Deleting model 'CellphoneScreenSize'
        db.delete_table('cotizador_cellphonescreensize')

        # Deleting model 'CellphoneScreenResolution'
        db.delete_table('cotizador_cellphonescreenresolution')

        # Deleting model 'CellphoneScreenColors'
        db.delete_table('cotizador_cellphonescreencolors')

        # Deleting model 'CellphoneScreen'
        db.delete_table('cotizador_cellphonescreen')

        # Deleting model 'CellphoneProcessor'
        db.delete_table('cotizador_cellphoneprocessor')

        # Deleting model 'Cellphone'
        db.delete_table('cotizador_cellphone')

        # Deleting model 'Cell'
        db.delete_table('cotizador_cell')

        # Deleting model 'RamBrand'
        db.delete_table('cotizador_rambrand')

        # Deleting model 'RamLine'
        db.delete_table('cotizador_ramline')

        # Deleting model 'RamMemoryBus'
        db.delete_table('cotizador_rammemorybus')

        # Deleting model 'RamFrequency'
        db.delete_table('cotizador_ramfrequency')

        # Deleting model 'RamBus'
        db.delete_table('cotizador_rambus')

        # Deleting model 'RamDimmCapacity'
        db.delete_table('cotizador_ramdimmcapacity')

        # Deleting model 'RamDimmQuantity'
        db.delete_table('cotizador_ramdimmquantity')

        # Deleting model 'RamTotalCapacity'
        db.delete_table('cotizador_ramtotalcapacity')

        # Deleting model 'RamCapacity'
        db.delete_table('cotizador_ramcapacity')

        # Deleting model 'RamVoltage'
        db.delete_table('cotizador_ramvoltage')

        # Deleting model 'RamLatencyCl'
        db.delete_table('cotizador_ramlatencycl')

        # Deleting model 'RamLatencyTras'
        db.delete_table('cotizador_ramlatencytras')

        # Deleting model 'RamLatencyTrcd'
        db.delete_table('cotizador_ramlatencytrcd')

        # Deleting model 'RamLatencyTrp'
        db.delete_table('cotizador_ramlatencytrp')

        # Deleting model 'Ram'
        db.delete_table('cotizador_ram')

        # Deleting model 'StorageDriveBrand'
        db.delete_table('cotizador_storagedrivebrand')

        # Deleting model 'StorageDriveBuffer'
        db.delete_table('cotizador_storagedrivebuffer')

        # Deleting model 'StorageDriveBus'
        db.delete_table('cotizador_storagedrivebus')

        # Deleting model 'StorageDriveCapacity'
        db.delete_table('cotizador_storagedrivecapacity')

        # Deleting model 'StorageDriveFamily'
        db.delete_table('cotizador_storagedrivefamily')

        # Deleting model 'StorageDriveLine'
        db.delete_table('cotizador_storagedriveline')

        # Deleting model 'StorageDriveRpm'
        db.delete_table('cotizador_storagedriverpm')

        # Deleting model 'StorageDriveSize'
        db.delete_table('cotizador_storagedrivesize')

        # Deleting model 'StorageDriveType'
        db.delete_table('cotizador_storagedrivetype')

        # Deleting model 'StorageDrive'
        db.delete_table('cotizador_storagedrive')

        # Deleting model 'PowerSupplyBrand'
        db.delete_table('cotizador_powersupplybrand')

        # Deleting model 'PowerSupplyLine'
        db.delete_table('cotizador_powersupplyline')

        # Deleting model 'PowerSupplyPower'
        db.delete_table('cotizador_powersupplypower')

        # Deleting model 'PowerSupplyCertification'
        db.delete_table('cotizador_powersupplycertification')

        # Deleting model 'PowerSupplySize'
        db.delete_table('cotizador_powersupplysize')

        # Deleting model 'PowerSupplyPowerConnector'
        db.delete_table('cotizador_powersupplypowerconnector')

        # Deleting model 'PowerSupplyHasPowerConnector'
        db.delete_table('cotizador_powersupplyhaspowerconnector')

        # Deleting model 'PowerSupply'
        db.delete_table('cotizador_powersupply')

        # Removing M2M table for field power_connectors on 'PowerSupply'
        db.delete_table('cotizador_powersupply_power_connectors')

        # Deleting model 'ComputerCaseFan'
        db.delete_table('cotizador_computercasefan')

        # Deleting model 'ComputerCaseBrand'
        db.delete_table('cotizador_computercasebrand')

        # Deleting model 'ComputerCaseFanDistribution'
        db.delete_table('cotizador_computercasefandistribution')

        # Deleting model 'ComputerCaseMotherboardFormat'
        db.delete_table('cotizador_computercasemotherboardformat')

        # Deleting model 'ComputerCasePowerSupply'
        db.delete_table('cotizador_computercasepowersupply')

        # Deleting model 'ComputerCasePowerSupplyPosition'
        db.delete_table('cotizador_computercasepowersupplyposition')

        # Deleting model 'ComputerCase'
        db.delete_table('cotizador_computercase')

        # Removing M2M table for field frontal_fan_slots on 'ComputerCase'
        db.delete_table('cotizador_computercase_frontal_fan_slots')

        # Removing M2M table for field rear_fan_slots on 'ComputerCase'
        db.delete_table('cotizador_computercase_rear_fan_slots')

        # Removing M2M table for field side_fan_slots on 'ComputerCase'
        db.delete_table('cotizador_computercase_side_fan_slots')

        # Removing M2M table for field top_fan_slots on 'ComputerCase'
        db.delete_table('cotizador_computercase_top_fan_slots')

        # Removing M2M table for field bottom_fan_slots on 'ComputerCase'
        db.delete_table('cotizador_computercase_bottom_fan_slots')

        # Removing M2M table for field included_fans on 'ComputerCase'
        db.delete_table('cotizador_computercase_included_fans')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'cotizador.advertisement': {
            'Meta': {'object_name': 'Advertisement'},
            'embedding_html': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'impressions': ('django.db.models.fields.IntegerField', [], {}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'position': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.AdvertisementPosition']"}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.Store']", 'null': 'True'}),
            'target_url': ('django.db.models.fields.TextField', [], {})
        },
        'cotizador.advertisementimpression': {
            'Meta': {'ordering': "['-date']", 'object_name': 'AdvertisementImpression'},
            'advertisement': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.Advertisement']"}),
            'date': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'db_index': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.advertisementimpressionperday': {
            'Meta': {'ordering': "['-date']", 'unique_together': "(('advertisement', 'date'),)", 'object_name': 'AdvertisementImpressionPerDay'},
            'advertisement': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.Advertisement']"}),
            'count': ('django.db.models.fields.IntegerField', [], {}),
            'date': ('django.db.models.fields.DateField', [], {'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.advertisementposition': {
            'Meta': {'object_name': 'AdvertisementPosition'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.advertisementvisit': {
            'Meta': {'object_name': 'AdvertisementVisit'},
            'advertisement': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.Advertisement']"}),
            'date': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'referer_url': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.cell': {
            'Meta': {'ordering': "['display_name']", 'object_name': 'Cell', '_ormbases': ['cotizador.Product']},
            'phone': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.Cellphone']"}),
            'pricing': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cotizador.CellPricing']", 'unique': 'True'}),
            'product_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cotizador.Product']", 'unique': 'True', 'primary_key': 'True'})
        },
        'cotizador.cellcompany': {
            'Meta': {'ordering': "['store']", 'object_name': 'CellCompany'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.Store']"})
        },
        'cotizador.cellphone': {
            'Meta': {'ordering': "['manufacturer', 'name']", 'object_name': 'Cellphone'},
            'battery': ('django.db.models.fields.IntegerField', [], {}),
            'camera': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.CellphoneCamera']"}),
            'card_reader': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.CellphoneCardReader']"}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.CellphoneCategory']"}),
            'depth': ('django.db.models.fields.IntegerField', [], {}),
            'form_factor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.CellphoneFormFactor']"}),
            'graphics': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.CellphoneGraphics']", 'null': 'True', 'blank': 'True'}),
            'has_3g': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'has_bluetooth': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'has_gps': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'has_headphones_output': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'has_wifi': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'height': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'internal_memory': ('django.db.models.fields.IntegerField', [], {}),
            'keyboard': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.CellphoneKeyboard']"}),
            'manufacturer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.CellphoneManufacturer']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'operating_system': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.CellphoneOperatingSystem']"}),
            'plays_mp3': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'processor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.CellphoneProcessor']", 'null': 'True', 'blank': 'True'}),
            'ram': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.CellphoneRam']"}),
            'records_video': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'screen': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.CellphoneScreen']"}),
            'weight': ('django.db.models.fields.IntegerField', [], {}),
            'width': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.cellphonecamera': {
            'Meta': {'ordering': "['mp']", 'object_name': 'CellphoneCamera'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mp': ('django.db.models.fields.DecimalField', [], {'max_digits': '3', 'decimal_places': '1'})
        },
        'cotizador.cellphonecardreader': {
            'Meta': {'ordering': "['name']", 'object_name': 'CellphoneCardReader'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.cellphonecategory': {
            'Meta': {'ordering': "['name']", 'object_name': 'CellphoneCategory'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.cellphoneformfactor': {
            'Meta': {'ordering': "['name']", 'object_name': 'CellphoneFormFactor'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.cellphonegraphics': {
            'Meta': {'ordering': "['name']", 'object_name': 'CellphoneGraphics'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.cellphonekeyboard': {
            'Meta': {'ordering': "['name']", 'object_name': 'CellphoneKeyboard'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.cellphonemanufacturer': {
            'Meta': {'ordering': "['brand']", 'object_name': 'CellphoneManufacturer'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.cellphoneoperatingsystem': {
            'Meta': {'ordering': "['name']", 'object_name': 'CellphoneOperatingSystem'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.cellphoneprocessor': {
            'Meta': {'ordering': "['name']", 'object_name': 'CellphoneProcessor'},
            'frequency': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.cellphoneram': {
            'Meta': {'ordering': "['value']", 'object_name': 'CellphoneRam'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.cellphonescreen': {
            'Meta': {'ordering': "['size', 'resolution']", 'object_name': 'CellphoneScreen'},
            'colors': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.CellphoneScreenColors']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_touch': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'resolution': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.CellphoneScreenResolution']"}),
            'size': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.CellphoneScreenSize']"})
        },
        'cotizador.cellphonescreencolors': {
            'Meta': {'ordering': "['quantity']", 'object_name': 'CellphoneScreenColors'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'quantity': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.cellphonescreenresolution': {
            'Meta': {'ordering': "['total_pixels']", 'object_name': 'CellphoneScreenResolution'},
            'height': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'total_pixels': ('django.db.models.fields.IntegerField', [], {}),
            'width': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.cellphonescreensize': {
            'Meta': {'ordering': "['value']", 'object_name': 'CellphoneScreenSize'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.DecimalField', [], {'max_digits': '2', 'decimal_places': '1'})
        },
        'cotizador.cellpricing': {
            'Meta': {'ordering': "['company', 'name']", 'object_name': 'CellPricing'},
            'best_tier': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.CellPricingTier']", 'null': 'True', 'blank': 'True'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.CellCompany']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.cellpricingplan': {
            'Meta': {'ordering': "['ordering', 'id']", 'object_name': 'CellPricingPlan'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.CellCompany']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'includes_data': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'default': '2'}),
            'price': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.cellpricingtier': {
            'Meta': {'ordering': "['pricing', 'plan']", 'object_name': 'CellPricingTier'},
            'cellphone_price': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'monthly_quota': ('django.db.models.fields.IntegerField', [], {}),
            'ordering_cellphone_price': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'ordering_six_month_price': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'ordering_three_month_price': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'ordering_twelve_month_price': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'plan': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.CellPricingPlan']"}),
            'pricing': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.CellPricing']"}),
            'shpe': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.StoreHasProductEntity']", 'null': 'True', 'blank': 'True'}),
            'six_month_pricing': ('django.db.models.fields.IntegerField', [], {}),
            'three_month_pricing': ('django.db.models.fields.IntegerField', [], {}),
            'twelve_month_pricing': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.computercase': {
            'Meta': {'ordering': "['display_name']", 'object_name': 'ComputerCase', '_ormbases': ['cotizador.Product']},
            'bottom_fan_slots': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'bfs'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['cotizador.ComputerCaseFanDistribution']"}),
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ComputerCaseBrand']"}),
            'external_3_1_2_bays': ('django.db.models.fields.IntegerField', [], {}),
            'external_5_1_4_bays': ('django.db.models.fields.IntegerField', [], {}),
            'front_usb_ports': ('django.db.models.fields.IntegerField', [], {}),
            'frontal_fan_slots': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'ffs'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['cotizador.ComputerCaseFanDistribution']"}),
            'has_front_audio_ports': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'has_front_esata_port': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'has_front_firewire_port': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'has_motherboard_tray': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'height': ('django.db.models.fields.IntegerField', [], {}),
            'included_fans': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'if'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['cotizador.ComputerCaseFanDistribution']"}),
            'internal_2_1_2_bays': ('django.db.models.fields.IntegerField', [], {}),
            'internal_3_1_2_bays': ('django.db.models.fields.IntegerField', [], {}),
            'largest_motherboard_format': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ComputerCaseMotherboardFormat']"}),
            'length': ('django.db.models.fields.IntegerField', [], {}),
            'power_supply': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ComputerCasePowerSupply']"}),
            'power_supply_position': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ComputerCasePowerSupplyPosition']"}),
            'product_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cotizador.Product']", 'unique': 'True', 'primary_key': 'True'}),
            'rear_expansion_slots': ('django.db.models.fields.IntegerField', [], {}),
            'rear_fan_slots': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'rfs'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['cotizador.ComputerCaseFanDistribution']"}),
            'side_fan_slots': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'sfs'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['cotizador.ComputerCaseFanDistribution']"}),
            'top_fan_slots': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'tfs'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['cotizador.ComputerCaseFanDistribution']"}),
            'weight': ('django.db.models.fields.IntegerField', [], {}),
            'width': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.computercasebrand': {
            'Meta': {'ordering': "['brand']", 'object_name': 'ComputerCaseBrand'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.computercasefan': {
            'Meta': {'ordering': "['mm']", 'object_name': 'ComputerCaseFan'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mm': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.computercasefandistribution': {
            'Meta': {'ordering': "['fan', 'quantity']", 'object_name': 'ComputerCaseFanDistribution'},
            'fan': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ComputerCaseFan']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'quantity': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.computercasemotherboardformat': {
            'Meta': {'object_name': 'ComputerCaseMotherboardFormat'},
            'format': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceMotherboardFormat']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.computercasepowersupply': {
            'Meta': {'ordering': "['power']", 'object_name': 'ComputerCasePowerSupply'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'power': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.computercasepowersupplyposition': {
            'Meta': {'ordering': "['name']", 'object_name': 'ComputerCasePowerSupplyPosition'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.externalvisit': {
            'Meta': {'object_name': 'ExternalVisit'},
            'date': ('django.db.models.fields.DateField', [], {'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'shn': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.StoreHasProductEntity']"})
        },
        'cotizador.interfacebrand': {
            'Meta': {'ordering': "['name']", 'object_name': 'InterfaceBrand'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.interfacebus': {
            'Meta': {'ordering': "['name']", 'object_name': 'InterfaceBus'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.interfacecardbus': {
            'Meta': {'ordering': "['name', 'version', 'lane']", 'object_name': 'InterfaceCardBus'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lane': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceCardBusLane']"}),
            'name': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceCardBusName']"}),
            'version': ('django.db.models.fields.DecimalField', [], {'max_digits': '2', 'decimal_places': '1'})
        },
        'cotizador.interfacecardbuslane': {
            'Meta': {'ordering': "['value']", 'object_name': 'InterfaceCardBusLane'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.interfacecardbusname': {
            'Meta': {'ordering': "['name']", 'object_name': 'InterfaceCardBusName'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'show_version_and_lanes': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'cotizador.interfacememorybus': {
            'Meta': {'ordering': "['format', 'type']", 'object_name': 'InterfaceMemoryBus'},
            'format': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceMemoryFormat']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pincount': ('django.db.models.fields.IntegerField', [], {}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceMemoryType']"})
        },
        'cotizador.interfacememoryformat': {
            'Meta': {'ordering': "['name']", 'object_name': 'InterfaceMemoryFormat'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.interfacememorytype': {
            'Meta': {'ordering': "['name']", 'object_name': 'InterfaceMemoryType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.interfacemotherboardformat': {
            'Meta': {'object_name': 'InterfaceMotherboardFormat'},
            'height': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'width': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.interfaceport': {
            'Meta': {'ordering': "['name']", 'object_name': 'InterfacePort'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.interfacepowerconnector': {
            'Meta': {'ordering': "['name']", 'object_name': 'InterfacePowerConnector'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.interfacesocket': {
            'Meta': {'ordering': "['brand', 'name']", 'object_name': 'InterfaceSocket'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceSocketBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'num_pins': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.interfacesocketbrand': {
            'Meta': {'ordering': "['brand']", 'object_name': 'InterfaceSocketBrand'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.interfacevideoport': {
            'Meta': {'ordering': "['name']", 'object_name': 'InterfaceVideoPort'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.logchangeentityname': {
            'Meta': {'object_name': 'LogChangeEntityName'},
            'entity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.StoreHasProductEntity']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'log_entry': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.LogEntry']"}),
            'new_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'old_name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.logchangeentityprice': {
            'Meta': {'object_name': 'LogChangeEntityPrice'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'log_entry': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.LogEntry']"}),
            'new_price': ('django.db.models.fields.IntegerField', [], {}),
            'old_price': ('django.db.models.fields.IntegerField', [], {}),
            'shpe': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.StoreHasProductEntity']"})
        },
        'cotizador.logchangeproductprice': {
            'Meta': {'object_name': 'LogChangeProductPrice'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'log_entry': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.LogEntry']"}),
            'new_price': ('django.db.models.fields.IntegerField', [], {}),
            'old_price': ('django.db.models.fields.IntegerField', [], {}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.Product']"})
        },
        'cotizador.logentry': {
            'Meta': {'object_name': 'LogEntry'},
            'date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.logentrymessage': {
            'Meta': {'object_name': 'LogEntryMessage'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logEntry': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.LogEntry']"}),
            'message': ('django.db.models.fields.TextField', [], {})
        },
        'cotizador.logfetchstoreerror': {
            'Meta': {'object_name': 'LogFetchStoreError'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'log_entry': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.LogEntry']"}),
            'message': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.Store']"})
        },
        'cotizador.loglostentity': {
            'Meta': {'object_name': 'LogLostEntity'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'log_entry': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.LogEntry']"}),
            'shpe': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.StoreHasProductEntity']"})
        },
        'cotizador.loglostproduct': {
            'Meta': {'object_name': 'LogLostProduct'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'log_entry': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.LogEntry']"}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.Product']"})
        },
        'cotizador.lognewentity': {
            'Meta': {'object_name': 'LogNewEntity'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'log_entry': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.LogEntry']"}),
            'shpe': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.StoreHasProductEntity']"})
        },
        'cotizador.logreviveentity': {
            'Meta': {'object_name': 'LogReviveEntity'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'log_entry': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.LogEntry']"}),
            'shpe': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.StoreHasProductEntity']"})
        },
        'cotizador.logreviveproduct': {
            'Meta': {'object_name': 'LogReviveProduct'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'log_entry': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.LogEntry']"}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.Product']"})
        },
        'cotizador.mailchangeproductprice': {
            'Meta': {'object_name': 'MailChangeProductPrice'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'log': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.LogChangeProductPrice']"}),
            'subscription': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ProductSubscription']"}),
            'success': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'cotizador.maillostproduct': {
            'Meta': {'object_name': 'MailLostProduct'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'log': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.LogLostProduct']"}),
            'subscription': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ProductSubscription']"}),
            'success': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'cotizador.mailreviveproduct': {
            'Meta': {'object_name': 'MailReviveProduct'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'log': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.LogReviveProduct']"}),
            'subscription': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ProductSubscription']"}),
            'success': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'cotizador.motherboard': {
            'Meta': {'ordering': "['display_name']", 'object_name': 'Motherboard', '_ormbases': ['cotizador.Product']},
            'allows_cf': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'allows_raid': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'allows_sli': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'audio_channels': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.MotherboardAudioChannels']"}),
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.MotherboardBrand']"}),
            'buses': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['cotizador.MotherboardHasBus']", 'symmetrical': 'False'}),
            'card_buses': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['cotizador.MotherboardHasCardBus']", 'symmetrical': 'False'}),
            'chipset': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.MotherboardChipset']"}),
            'format': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.MotherboardFormat']"}),
            'memory_channels': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.MotherboardMemoryChannel']"}),
            'memory_types': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['cotizador.MotherboardHasMemoryType']", 'symmetrical': 'False'}),
            'ports': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['cotizador.MotherboardHasPort']", 'symmetrical': 'False'}),
            'power_connectors': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['cotizador.MotherboardHasPowerConnector']", 'symmetrical': 'False'}),
            'product_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cotizador.Product']", 'unique': 'True', 'primary_key': 'True'}),
            'video_ports': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['cotizador.MotherboardHasVideoPort']", 'null': 'True', 'blank': 'True'})
        },
        'cotizador.motherboardaudiochannels': {
            'Meta': {'ordering': "['name']", 'object_name': 'MotherboardAudioChannels'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.motherboardbrand': {
            'Meta': {'ordering': "['brand']", 'object_name': 'MotherboardBrand'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.motherboardbus': {
            'Meta': {'ordering': "['bus']", 'object_name': 'MotherboardBus'},
            'bus': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceBus']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.motherboardcardbus': {
            'Meta': {'ordering': "['bus']", 'object_name': 'MotherboardCardBus'},
            'bus': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceCardBus']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.motherboardchipset': {
            'Meta': {'ordering': "['northbridge']", 'object_name': 'MotherboardChipset'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'northbridge': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.MotherboardNorthbridge']"}),
            'southbridge': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.MotherboardSouthbridge']"})
        },
        'cotizador.motherboardchipsetbrand': {
            'Meta': {'ordering': "['brand']", 'object_name': 'MotherboardChipsetBrand'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.motherboardformat': {
            'Meta': {'object_name': 'MotherboardFormat'},
            'format': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceMotherboardFormat']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.motherboardgraphics': {
            'Meta': {'ordering': "['name']", 'object_name': 'MotherboardGraphics'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.motherboardhasbus': {
            'Meta': {'ordering': "['bus', 'quantity']", 'object_name': 'MotherboardHasBus'},
            'bus': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.MotherboardBus']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'quantity': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.motherboardhascardbus': {
            'Meta': {'ordering': "['bus', 'quantity']", 'object_name': 'MotherboardHasCardBus'},
            'bus': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.MotherboardCardBus']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'quantity': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.motherboardhasmemorytype': {
            'Meta': {'ordering': "['mtype']", 'object_name': 'MotherboardHasMemoryType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mtype': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.MotherboardMemoryType']"}),
            'quantity': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.motherboardhasport': {
            'Meta': {'ordering': "['port', 'quantity']", 'object_name': 'MotherboardHasPort'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'port': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.MotherboardPort']"}),
            'quantity': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.motherboardhaspowerconnector': {
            'Meta': {'ordering': "['connector', 'quantity']", 'object_name': 'MotherboardHasPowerConnector'},
            'connector': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.MotherboardPowerConnector']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'quantity': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.motherboardhasvideoport': {
            'Meta': {'ordering': "['port', 'quantity']", 'object_name': 'MotherboardHasVideoPort'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'port': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.MotherboardVideoPort']"}),
            'quantity': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.motherboardmemorychannel': {
            'Meta': {'ordering': "['value']", 'object_name': 'MotherboardMemoryChannel'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.motherboardmemorytype': {
            'Meta': {'ordering': "['itype']", 'object_name': 'MotherboardMemoryType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'itype': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceMemoryBus']", 'null': 'True', 'blank': 'True'})
        },
        'cotizador.motherboardnorthbridge': {
            'Meta': {'ordering': "['family']", 'object_name': 'MotherboardNorthbridge'},
            'family': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.MotherboardNorthbridgeFamily']"}),
            'graphics': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.MotherboardGraphics']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.motherboardnorthbridgefamily': {
            'Meta': {'ordering': "['brand', 'name']", 'object_name': 'MotherboardNorthbridgeFamily'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.MotherboardChipsetBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'socket': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.MotherboardSocket']"})
        },
        'cotizador.motherboardport': {
            'Meta': {'ordering': "['port']", 'object_name': 'MotherboardPort'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'port': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfacePort']"})
        },
        'cotizador.motherboardpowerconnector': {
            'Meta': {'ordering': "['connector']", 'object_name': 'MotherboardPowerConnector'},
            'connector': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfacePowerConnector']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.motherboardsocket': {
            'Meta': {'ordering': "['socket']", 'object_name': 'MotherboardSocket'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'socket': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceSocket']"})
        },
        'cotizador.motherboardsouthbridge': {
            'Meta': {'object_name': 'MotherboardSouthbridge'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.motherboardvideoport': {
            'Meta': {'ordering': "['port']", 'object_name': 'MotherboardVideoPort'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'port': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceVideoPort']"})
        },
        'cotizador.notebook': {
            'Meta': {'ordering': "['display_name']", 'object_name': 'Notebook', '_ormbases': ['cotizador.Product']},
            'battery_cells': ('django.db.models.fields.IntegerField', [], {}),
            'battery_mah': ('django.db.models.fields.IntegerField', [], {}),
            'battery_mv': ('django.db.models.fields.IntegerField', [], {}),
            'battery_mwh': ('django.db.models.fields.IntegerField', [], {}),
            'card_reader': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookCardReader']"}),
            'chipset': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookChipset']"}),
            'has_bluetooth': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'has_esata': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'has_fingerprint_reader': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'has_firewire': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'height': ('django.db.models.fields.IntegerField', [], {}),
            'is_ram_dual_channel': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'lan': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookLan']"}),
            'line': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookLine']"}),
            'ntype': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookType']"}),
            'operating_system': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookOperatingSystem']"}),
            'optical_drive': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookOpticalDrive']"}),
            'power_adapter': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookPowerAdapter']"}),
            'processor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookProcessor']"}),
            'product_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cotizador.Product']", 'unique': 'True', 'primary_key': 'True'}),
            'ram_frequency': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookRamFrequency']"}),
            'ram_quantity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookRamQuantity']"}),
            'ram_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookRamType']"}),
            'screen': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookScreen']"}),
            'storage_drive': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['cotizador.NotebookStorageDrive']", 'symmetrical': 'False'}),
            'thickness': ('django.db.models.fields.IntegerField', [], {}),
            'usb_port_count': ('django.db.models.fields.IntegerField', [], {}),
            'video_card': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['cotizador.NotebookVideoCard']", 'symmetrical': 'False'}),
            'video_port': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['cotizador.NotebookVideoPort']", 'symmetrical': 'False'}),
            'webcam_mp': ('django.db.models.fields.DecimalField', [], {'max_digits': '3', 'decimal_places': '1'}),
            'weight': ('django.db.models.fields.IntegerField', [], {}),
            'width': ('django.db.models.fields.IntegerField', [], {}),
            'wifi_card': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookWifiCard']"})
        },
        'cotizador.notebookbrand': {
            'Meta': {'ordering': "['brand']", 'object_name': 'NotebookBrand'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.notebookcardreader': {
            'Meta': {'ordering': "['name']", 'object_name': 'NotebookCardReader'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.notebookchipset': {
            'Meta': {'ordering': "['brand', 'name']", 'object_name': 'NotebookChipset'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookChipsetBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.notebookchipsetbrand': {
            'Meta': {'ordering': "['brand']", 'object_name': 'NotebookChipsetBrand'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.notebooklan': {
            'Meta': {'object_name': 'NotebookLan'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.notebookline': {
            'Meta': {'ordering': "['brand', 'name']", 'object_name': 'NotebookLine'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.notebookoperatingsystem': {
            'Meta': {'ordering': "['family', 'name']", 'object_name': 'NotebookOperatingSystem'},
            'family': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookOperatingSystemFamily']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_64_bit': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'language': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookOperatingSystemLanguage']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.notebookoperatingsystembrand': {
            'Meta': {'ordering': "['brand']", 'object_name': 'NotebookOperatingSystemBrand'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.notebookoperatingsystemfamily': {
            'Meta': {'ordering': "['brand', 'name']", 'object_name': 'NotebookOperatingSystemFamily'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookOperatingSystemBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.notebookoperatingsystemlanguage': {
            'Meta': {'object_name': 'NotebookOperatingSystemLanguage'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.notebookopticaldrive': {
            'Meta': {'object_name': 'NotebookOpticalDrive'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.notebookpoweradapter': {
            'Meta': {'ordering': "['power']", 'object_name': 'NotebookPowerAdapter'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'power': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.notebookprocessor': {
            'Meta': {'ordering': "('line', 'name')", 'object_name': 'NotebookProcessor'},
            'cache': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookProcessorCache']"}),
            'consumption': ('django.db.models.fields.IntegerField', [], {}),
            'core_number': ('django.db.models.fields.IntegerField', [], {}),
            'family': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookProcessorFamily']"}),
            'frequency': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookProcessorFrequency']"}),
            'fsb': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookProcessorFSB']"}),
            'has_smp': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'has_turbo_mode': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'line': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookProcessorLine']"}),
            'max_voltage': ('django.db.models.fields.IntegerField', [], {}),
            'min_voltage': ('django.db.models.fields.IntegerField', [], {}),
            'multiplier': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookProcessorMultiplier']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'socket': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookProcessorSocket']"}),
            'speed_score': ('django.db.models.fields.IntegerField', [], {}),
            'tdp': ('django.db.models.fields.DecimalField', [], {'max_digits': '4', 'decimal_places': '1'})
        },
        'cotizador.notebookprocessorbrand': {
            'Meta': {'ordering': "['brand']", 'object_name': 'NotebookProcessorBrand'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.notebookprocessorcache': {
            'Meta': {'ordering': "('value',)", 'object_name': 'NotebookProcessorCache'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.notebookprocessorfamily': {
            'Meta': {'ordering': "['name']", 'object_name': 'NotebookProcessorFamily'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'nm': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookProcessorManufacturing']"})
        },
        'cotizador.notebookprocessorfrequency': {
            'Meta': {'ordering': "('value',)", 'object_name': 'NotebookProcessorFrequency'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.notebookprocessorfsb': {
            'Meta': {'ordering': "('value',)", 'object_name': 'NotebookProcessorFSB'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.notebookprocessorline': {
            'Meta': {'ordering': "['family', 'name']", 'object_name': 'NotebookProcessorLine'},
            'family': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookProcessorLineFamily']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.notebookprocessorlinefamily': {
            'Meta': {'ordering': "['brand', 'name']", 'object_name': 'NotebookProcessorLineFamily'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookProcessorBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        'cotizador.notebookprocessormanufacturing': {
            'Meta': {'ordering': "['value']", 'object_name': 'NotebookProcessorManufacturing'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.notebookprocessormultiplier': {
            'Meta': {'ordering': "['value']", 'object_name': 'NotebookProcessorMultiplier'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.DecimalField', [], {'max_digits': '3', 'decimal_places': '1'})
        },
        'cotizador.notebookprocessorsocket': {
            'Meta': {'ordering': "['name']", 'object_name': 'NotebookProcessorSocket'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'pincount': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.notebookramfrequency': {
            'Meta': {'ordering': "('value',)", 'object_name': 'NotebookRamFrequency'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.notebookramquantity': {
            'Meta': {'ordering': "['value']", 'object_name': 'NotebookRamQuantity'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.DecimalField', [], {'max_digits': '3', 'decimal_places': '1'})
        },
        'cotizador.notebookramtype': {
            'Meta': {'ordering': "['name']", 'object_name': 'NotebookRamType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.notebookscreen': {
            'Meta': {'ordering': "['size', 'resolution']", 'object_name': 'NotebookScreen'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_glossy': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_led': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_rotating': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_touchscreen': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'resolution': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookScreenResolution']"}),
            'size': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookScreenSize']"})
        },
        'cotizador.notebookscreenresolution': {
            'Meta': {'ordering': "('horizontal', 'vertical')", 'object_name': 'NotebookScreenResolution'},
            'horizontal': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'vertical': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.notebookscreensize': {
            'Meta': {'ordering': "('size',)", 'object_name': 'NotebookScreenSize'},
            'family': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookScreenSizeFamily']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'size': ('django.db.models.fields.DecimalField', [], {'max_digits': '3', 'decimal_places': '1'})
        },
        'cotizador.notebookscreensizefamily': {
            'Meta': {'ordering': "('base_size',)", 'object_name': 'NotebookScreenSizeFamily'},
            'base_size': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.notebookstoragedrive': {
            'Meta': {'ordering': "['drive_type', 'capacity', 'rpm']", 'object_name': 'NotebookStorageDrive'},
            'capacity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookStorageDriveCapacity']"}),
            'drive_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookStorageDriveType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'read_speed': ('django.db.models.fields.IntegerField', [], {}),
            'rpm': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookStorageDriveRpm']"}),
            'write_speed': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.notebookstoragedrivecapacity': {
            'Meta': {'ordering': "['value']", 'object_name': 'NotebookStorageDriveCapacity'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.notebookstoragedriverpm': {
            'Meta': {'ordering': "['value']", 'object_name': 'NotebookStorageDriveRpm'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.notebookstoragedrivetype': {
            'Meta': {'object_name': 'NotebookStorageDriveType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.notebooktype': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'NotebookType'},
            'css_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {}),
            'processor_consumption': ('django.db.models.fields.IntegerField', [], {}),
            'processor_speed': ('django.db.models.fields.IntegerField', [], {}),
            'ram_quantity': ('django.db.models.fields.IntegerField', [], {}),
            'screen_size': ('django.db.models.fields.IntegerField', [], {}),
            'storage_quantity': ('django.db.models.fields.IntegerField', [], {}),
            'video_card_speed': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.notebookvideocard': {
            'Meta': {'ordering': "['line', 'name']", 'object_name': 'NotebookVideoCard'},
            'card_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookVideoCardType']"}),
            'gpu_frequency': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'line': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookVideoCardLine']"}),
            'memory': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookVideoCardMemory']"}),
            'memory_frequency': ('django.db.models.fields.IntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'speed_score': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.notebookvideocardbrand': {
            'Meta': {'ordering': "['brand']", 'object_name': 'NotebookVideoCardBrand'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.notebookvideocardline': {
            'Meta': {'ordering': "['brand', 'name']", 'object_name': 'NotebookVideoCardLine'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookVideoCardBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.notebookvideocardmemory': {
            'Meta': {'object_name': 'NotebookVideoCardMemory'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.notebookvideocardtype': {
            'Meta': {'object_name': 'NotebookVideoCardType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.notebookvideoport': {
            'Meta': {'ordering': "['name']", 'object_name': 'NotebookVideoPort'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.notebookwificard': {
            'Meta': {'ordering': "['brand', 'name', 'norm']", 'object_name': 'NotebookWifiCard'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookWifiCardBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'norm': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.NotebookWifiCardNorm']"})
        },
        'cotizador.notebookwificardbrand': {
            'Meta': {'ordering': "['brand']", 'object_name': 'NotebookWifiCardBrand'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.notebookwificardnorm': {
            'Meta': {'object_name': 'NotebookWifiCardNorm'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.powersupply': {
            'Meta': {'ordering': "['display_name']", 'object_name': 'PowerSupply', '_ormbases': ['cotizador.Product']},
            'certification': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.PowerSupplyCertification']"}),
            'currents_on_12V_rails': ('django.db.models.fields.CommaSeparatedIntegerField', [], {'max_length': '255'}),
            'currents_on_33V_rails': ('django.db.models.fields.CommaSeparatedIntegerField', [], {'max_length': '255'}),
            'currents_on_5V_rails': ('django.db.models.fields.CommaSeparatedIntegerField', [], {'max_length': '255'}),
            'has_active_pfc': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_modular': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'line': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.PowerSupplyLine']"}),
            'power': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.PowerSupplyPower']"}),
            'power_connectors': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['cotizador.PowerSupplyHasPowerConnector']", 'symmetrical': 'False'}),
            'product_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cotizador.Product']", 'unique': 'True', 'primary_key': 'True'}),
            'size': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.PowerSupplySize']"})
        },
        'cotizador.powersupplybrand': {
            'Meta': {'ordering': "['brand']", 'object_name': 'PowerSupplyBrand'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.powersupplycertification': {
            'Meta': {'ordering': "['value']", 'object_name': 'PowerSupplyCertification'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.powersupplyhaspowerconnector': {
            'Meta': {'ordering': "['connector', 'quantity']", 'object_name': 'PowerSupplyHasPowerConnector'},
            'connector': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.PowerSupplyPowerConnector']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'quantity': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.powersupplyline': {
            'Meta': {'ordering': "['brand', 'name']", 'object_name': 'PowerSupplyLine'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.PowerSupplyBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.powersupplypower': {
            'Meta': {'ordering': "['value']", 'object_name': 'PowerSupplyPower'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.powersupplypowerconnector': {
            'Meta': {'ordering': "['connector']", 'object_name': 'PowerSupplyPowerConnector'},
            'connector': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfacePowerConnector']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.powersupplysize': {
            'Meta': {'ordering': "['name']", 'object_name': 'PowerSupplySize'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.processor': {
            'Meta': {'ordering': "['display_name']", 'object_name': 'Processor', '_ormbases': ['cotizador.Product']},
            'core': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ProcessorCore']"}),
            'core_count': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ProcessorCoreCount']"}),
            'frequency': ('django.db.models.fields.IntegerField', [], {}),
            'fsb': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ProcessorFsb']"}),
            'graphics': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ProcessorGraphics']"}),
            'has_smp': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'has_unlocked_multiplier': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'has_vt': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_64_bit': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'l2_cache': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ProcessorL2Cache']"}),
            'l3_cache': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ProcessorL3Cache']"}),
            'line': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ProcessorLine']"}),
            'max_voltage': ('django.db.models.fields.IntegerField', [], {}),
            'min_voltage': ('django.db.models.fields.IntegerField', [], {}),
            'multiplier': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ProcessorMultiplier']"}),
            'passmark_score': ('django.db.models.fields.IntegerField', [], {}),
            'pcmark_05_score': ('django.db.models.fields.IntegerField', [], {}),
            'pcmark_id': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'pcmark_vantage_score': ('django.db.models.fields.IntegerField', [], {}),
            'product_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cotizador.Product']", 'unique': 'True', 'primary_key': 'True'}),
            'socket': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ProcessorSocket']"}),
            'tdp': ('django.db.models.fields.IntegerField', [], {}),
            'turbo_modes': ('django.db.models.fields.CommaSeparatedIntegerField', [], {'max_length': '50'})
        },
        'cotizador.processorarchitecture': {
            'Meta': {'ordering': "['brand']", 'object_name': 'ProcessorArchitecture'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ProcessorBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'turbo_step': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.processorbrand': {
            'Meta': {'ordering': "['brand']", 'object_name': 'ProcessorBrand'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.processorcore': {
            'Meta': {'ordering': "['architecture', 'name']", 'object_name': 'ProcessorCore'},
            'architecture': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ProcessorArchitecture']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manufacturing_process': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ProcessorManufacturingProcess']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.processorcorecount': {
            'Meta': {'ordering': "['value']", 'object_name': 'ProcessorCoreCount'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.processorfamily': {
            'Meta': {'ordering': "['brand', 'name']", 'object_name': 'ProcessorFamily'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ProcessorBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'separator': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.processorfsb': {
            'Meta': {'ordering': "['value']", 'object_name': 'ProcessorFsb'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.processorgraphics': {
            'Meta': {'object_name': 'ProcessorGraphics'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.processorl2cache': {
            'Meta': {'ordering': "['quantity', 'multiplier']", 'object_name': 'ProcessorL2Cache'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'multiplier': ('django.db.models.fields.IntegerField', [], {}),
            'quantity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ProcessorL2CacheQuantity']"})
        },
        'cotizador.processorl2cachequantity': {
            'Meta': {'ordering': "['value']", 'object_name': 'ProcessorL2CacheQuantity'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.processorl3cache': {
            'Meta': {'ordering': "['quantity', 'multiplier']", 'object_name': 'ProcessorL3Cache'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'multiplier': ('django.db.models.fields.IntegerField', [], {}),
            'quantity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ProcessorL3CacheQuantity']"})
        },
        'cotizador.processorl3cachequantity': {
            'Meta': {'ordering': "['value']", 'object_name': 'ProcessorL3CacheQuantity'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.processorline': {
            'Meta': {'ordering': "['family', 'name']", 'object_name': 'ProcessorLine'},
            'family': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ProcessorFamily']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.processormanufacturingprocess': {
            'Meta': {'ordering': "['value']", 'object_name': 'ProcessorManufacturingProcess'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.processormultiplier': {
            'Meta': {'ordering': "['value']", 'object_name': 'ProcessorMultiplier'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.DecimalField', [], {'max_digits': '3', 'decimal_places': '1'})
        },
        'cotizador.processorsocket': {
            'Meta': {'ordering': "['socket']", 'object_name': 'ProcessorSocket'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'socket': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceSocket']"})
        },
        'cotizador.product': {
            'Meta': {'ordering': "['display_name']", 'object_name': 'Product'},
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'display_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'long_description': ('django.db.models.fields.TextField', [], {'default': "' '"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'part_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'ptype': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ProductType']", 'null': 'True', 'blank': 'True'}),
            'review_url': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'shp': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'chosen_by'", 'null': 'True', 'to': "orm['cotizador.StoreHasProduct']"}),
            'similar_products': ('django.db.models.fields.CommaSeparatedIntegerField', [], {'default': "'0'", 'max_length': '30'}),
            'sponsored_shp': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'sponsored_product'", 'null': 'True', 'to': "orm['cotizador.StoreHasProduct']"}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'week_discount': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'week_external_visits': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'week_visitor_count': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'cotizador.productcomment': {
            'Meta': {'object_name': 'ProductComment'},
            'comments': ('django.db.models.fields.TextField', [], {}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nickname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'notebook': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.Product']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'validated': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'cotizador.productcomparisonlist': {
            'Meta': {'object_name': 'ProductComparisonList'},
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'products': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['cotizador.Product']", 'symmetrical': 'False'})
        },
        'cotizador.productpicture': {
            'Meta': {'object_name': 'ProductPicture'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notebook': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.Product']"}),
            'picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        },
        'cotizador.productpricechange': {
            'Meta': {'object_name': 'ProductPriceChange'},
            'date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notebook': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.Product']"}),
            'price': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.productsubscription': {
            'Meta': {'object_name': 'ProductSubscription'},
            'email_notifications': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.Product']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'cotizador.producttype': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'ProductType'},
            'adminurlname': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'classname': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'displayname': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'indexname': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {}),
            'urlname': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.productvisit': {
            'Meta': {'object_name': 'ProductVisit'},
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'db_index': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notebook': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.Product']"})
        },
        'cotizador.ram': {
            'Meta': {'ordering': "['display_name']", 'object_name': 'Ram', '_ormbases': ['cotizador.Product']},
            'bus': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.RamBus']"}),
            'capacity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.RamCapacity']"}),
            'is_ecc': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_fully_buffered': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'latency_cl': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.RamLatencyCl']", 'null': 'True', 'blank': 'True'}),
            'latency_tras': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.RamLatencyTras']", 'null': 'True', 'blank': 'True'}),
            'latency_trcd': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.RamLatencyTrcd']", 'null': 'True', 'blank': 'True'}),
            'latency_trp': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.RamLatencyTrp']", 'null': 'True', 'blank': 'True'}),
            'line': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.RamLine']"}),
            'product_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cotizador.Product']", 'unique': 'True', 'primary_key': 'True'}),
            'voltage': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.RamVoltage']"})
        },
        'cotizador.rambrand': {
            'Meta': {'ordering': "['brand']", 'object_name': 'RamBrand'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.rambus': {
            'Meta': {'ordering': "['bus', 'frequency']", 'object_name': 'RamBus'},
            'bus': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.RamMemoryBus']"}),
            'frequency': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.RamFrequency']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.ramcapacity': {
            'Meta': {'ordering': "['dimm_quantity', 'dimm_capacity']", 'object_name': 'RamCapacity'},
            'dimm_capacity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.RamDimmCapacity']"}),
            'dimm_quantity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.RamDimmQuantity']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'total_capacity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.RamTotalCapacity']"})
        },
        'cotizador.ramdimmcapacity': {
            'Meta': {'ordering': "['value']", 'object_name': 'RamDimmCapacity'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.ramdimmquantity': {
            'Meta': {'ordering': "['value']", 'object_name': 'RamDimmQuantity'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.ramfrequency': {
            'Meta': {'ordering': "['value']", 'object_name': 'RamFrequency'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.ramlatencycl': {
            'Meta': {'ordering': "['value']", 'object_name': 'RamLatencyCl'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.ramlatencytras': {
            'Meta': {'ordering': "['value']", 'object_name': 'RamLatencyTras'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.ramlatencytrcd': {
            'Meta': {'ordering': "['value']", 'object_name': 'RamLatencyTrcd'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.ramlatencytrp': {
            'Meta': {'ordering': "['value']", 'object_name': 'RamLatencyTrp'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.ramline': {
            'Meta': {'ordering': "['brand', 'name']", 'object_name': 'RamLine'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.RamBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.rammemorybus': {
            'Meta': {'ordering': "['bus']", 'object_name': 'RamMemoryBus'},
            'bus': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceMemoryBus']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.ramtotalcapacity': {
            'Meta': {'ordering': "['value']", 'object_name': 'RamTotalCapacity'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.ramvoltage': {
            'Meta': {'ordering': "['value']", 'object_name': 'RamVoltage'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.DecimalField', [], {'max_digits': '4', 'decimal_places': '2'})
        },
        'cotizador.screen': {
            'Meta': {'ordering': "['display_name']", 'object_name': 'Screen', '_ormbases': ['cotizador.Product']},
            'brightness': ('django.db.models.fields.IntegerField', [], {}),
            'consumption': ('django.db.models.fields.IntegerField', [], {}),
            'contrast': ('django.db.models.fields.IntegerField', [], {}),
            'digital_tuner': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ScreenDigitalTuner']"}),
            'display': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ScreenDisplay']"}),
            'has_analog_tuner': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_3d': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'line': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ScreenLine']"}),
            'panel_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ScreenPanelType']"}),
            'product_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cotizador.Product']", 'unique': 'True', 'primary_key': 'True'}),
            'refresh_rate': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ScreenRefreshRate']"}),
            'resolution': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ScreenResolution']"}),
            'response_time': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ScreenResponseTime']"}),
            'size': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ScreenSize']"}),
            'speakers': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ScreenSpeakers']"}),
            'stype': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ScreenType']"}),
            'usb_ports': ('django.db.models.fields.IntegerField', [], {}),
            'video_ports': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['cotizador.ScreenHasVideoPort']", 'symmetrical': 'False'})
        },
        'cotizador.screenaspectratio': {
            'Meta': {'object_name': 'ScreenAspectRatio'},
            'h_value': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'v_value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.screenbrand': {
            'Meta': {'ordering': "['brand']", 'object_name': 'ScreenBrand'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.screendigitaltuner': {
            'Meta': {'ordering': "['ordering']", 'object_name': 'ScreenDigitalTuner'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.screendisplay': {
            'Meta': {'ordering': "['dtype']", 'object_name': 'ScreenDisplay'},
            'backlight': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'dtype': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ScreenDisplayType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.screendisplaytype': {
            'Meta': {'ordering': "['name']", 'object_name': 'ScreenDisplayType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.screenhasvideoport': {
            'Meta': {'ordering': "['port', 'quantity']", 'object_name': 'ScreenHasVideoPort'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'port': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ScreenVideoPort']"}),
            'quantity': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.screenline': {
            'Meta': {'ordering': "['brand', 'name']", 'object_name': 'ScreenLine'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ScreenBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.screenpaneltype': {
            'Meta': {'ordering': "['name']", 'object_name': 'ScreenPanelType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.screenrefreshrate': {
            'Meta': {'ordering': "['value']", 'object_name': 'ScreenRefreshRate'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.screenresolution': {
            'Meta': {'ordering': "['-commercial_name', 'total_pixels']", 'object_name': 'ScreenResolution'},
            'aspect_ratio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ScreenAspectRatio']"}),
            'commercial_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'h_value': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'total_pixels': ('django.db.models.fields.IntegerField', [], {}),
            'v_value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.screenresponsetime': {
            'Meta': {'ordering': "['value']", 'object_name': 'ScreenResponseTime'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.screensize': {
            'Meta': {'ordering': "['value']", 'object_name': 'ScreenSize'},
            'family': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ScreenSizeFamily']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.DecimalField', [], {'max_digits': '4', 'decimal_places': '1'})
        },
        'cotizador.screensizefamily': {
            'Meta': {'ordering': "['value']", 'object_name': 'ScreenSizeFamily'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.screenspeakers': {
            'Meta': {'ordering': "['value']", 'object_name': 'ScreenSpeakers'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.screentype': {
            'Meta': {'ordering': "['name']", 'object_name': 'ScreenType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.screenvideoport': {
            'Meta': {'ordering': "['port']", 'object_name': 'ScreenVideoPort'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'port': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceVideoPort']"})
        },
        'cotizador.searchregistry': {
            'Meta': {'object_name': 'SearchRegistry'},
            'date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'query': ('django.db.models.fields.TextField', [], {})
        },
        'cotizador.sponsoredvisit': {
            'Meta': {'ordering': "['-date']", 'object_name': 'SponsoredVisit'},
            'date': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'shp': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.StoreHasProduct']"})
        },
        'cotizador.storagedrive': {
            'Meta': {'ordering': "['display_name']", 'object_name': 'StorageDrive', '_ormbases': ['cotizador.Product']},
            'buffer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.StorageDriveBuffer']"}),
            'bus': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.StorageDriveBus']"}),
            'capacity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.StorageDriveCapacity']"}),
            'line': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.StorageDriveLine']"}),
            'product_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cotizador.Product']", 'unique': 'True', 'primary_key': 'True'}),
            'random_read_speed': ('django.db.models.fields.IntegerField', [], {}),
            'random_write_speed': ('django.db.models.fields.IntegerField', [], {}),
            'rpm': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.StorageDriveRpm']"}),
            'sequential_read_speed': ('django.db.models.fields.IntegerField', [], {}),
            'sequential_write_speed': ('django.db.models.fields.IntegerField', [], {}),
            'size': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.StorageDriveSize']"}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.StorageDriveType']"})
        },
        'cotizador.storagedrivebrand': {
            'Meta': {'ordering': "['brand']", 'object_name': 'StorageDriveBrand'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.storagedrivebuffer': {
            'Meta': {'ordering': "['value']", 'object_name': 'StorageDriveBuffer'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.storagedrivebus': {
            'Meta': {'ordering': "['bus']", 'object_name': 'StorageDriveBus'},
            'bus': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceBus']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.storagedrivecapacity': {
            'Meta': {'ordering': "['value']", 'object_name': 'StorageDriveCapacity'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.storagedrivefamily': {
            'Meta': {'ordering': "['brand', 'name']", 'object_name': 'StorageDriveFamily'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.StorageDriveBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.storagedriveline': {
            'Meta': {'ordering': "['family', 'name']", 'object_name': 'StorageDriveLine'},
            'family': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.StorageDriveFamily']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.storagedriverpm': {
            'Meta': {'ordering': "['value']", 'object_name': 'StorageDriveRpm'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.storagedrivesize': {
            'Meta': {'ordering': "['value']", 'object_name': 'StorageDriveSize'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.DecimalField', [], {'max_digits': '3', 'decimal_places': '1'})
        },
        'cotizador.storagedrivetype': {
            'Meta': {'ordering': "['name']", 'object_name': 'StorageDriveType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.store': {
            'Meta': {'ordering': "['name']", 'object_name': 'Store'},
            'affiliate_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'classname': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'sponsor_cap': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        'cotizador.storecustomupdateregistry': {
            'Meta': {'ordering': "['-start_datetime', 'store']", 'object_name': 'StoreCustomUpdateRegistry'},
            'end_datetime': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'start_datetime': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.Store']"})
        },
        'cotizador.storehasproduct': {
            'Meta': {'ordering': "['product']", 'object_name': 'StoreHasProduct'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.Product']", 'null': 'True', 'blank': 'True'}),
            'shpe': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.StoreHasProductEntity']", 'null': 'True', 'blank': 'True'})
        },
        'cotizador.storehasproductentity': {
            'Meta': {'object_name': 'StoreHasProductEntity'},
            'comparison_field': ('django.db.models.fields.TextField', [], {}),
            'custom_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'date_added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_resolved': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'latest_price': ('django.db.models.fields.IntegerField', [], {}),
            'part_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'prevent_availability_change': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ptype': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.ProductType']", 'null': 'True', 'blank': 'True'}),
            'resolved_by': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'shp': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.StoreHasProduct']", 'null': 'True', 'blank': 'True'}),
            'store': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.Store']"}),
            'url': ('django.db.models.fields.TextField', [], {})
        },
        'cotizador.storeproducthistory': {
            'Meta': {'object_name': 'StoreProductHistory'},
            'date': ('django.db.models.fields.DateField', [], {'db_index': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.IntegerField', [], {}),
            'registry': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.StoreHasProductEntity']"})
        },
        'cotizador.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            'assigned_store': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.Store']", 'null': 'True', 'blank': 'True'}),
            'can_access_competitivity_report': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'can_use_extra_ordering_options': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'change_mails_sent': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'confirmation_mails_sent': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'facebook_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'managed_product_types': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['cotizador.ProductType']", 'symmetrical': 'False'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True'})
        },
        'cotizador.videocard': {
            'Meta': {'ordering': "['brand', 'gpu__name', 'name']", 'object_name': 'VideoCard', '_ormbases': ['cotizador.Product']},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.VideoCardBrand']"}),
            'bus': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.VideoCardBus']"}),
            'core_clock': ('django.db.models.fields.IntegerField', [], {}),
            'gpu': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.VideoCardGpu']"}),
            'memory_bus_width': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.VideoCardMemoryBusWidth']"}),
            'memory_clock': ('django.db.models.fields.IntegerField', [], {}),
            'memory_quantity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.VideoCardMemoryQuantity']"}),
            'memory_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.VideoCardMemoryType']"}),
            'product_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cotizador.Product']", 'unique': 'True', 'primary_key': 'True'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.VideoCardProfile']"}),
            'refrigeration': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.VideoCardRefrigeration']"}),
            'shader_clock': ('django.db.models.fields.IntegerField', [], {}),
            'slot_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.VideoCardSlotType']"}),
            'video_ports': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['cotizador.VideoCardHasPort']", 'symmetrical': 'False'})
        },
        'cotizador.videocardbrand': {
            'Meta': {'ordering': "['brand']", 'object_name': 'VideoCardBrand'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.videocardbus': {
            'Meta': {'ordering': "['bus']", 'object_name': 'VideoCardBus'},
            'bus': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceCardBus']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.videocardgpu': {
            'Meta': {'ordering': "['line__family', 'name']", 'object_name': 'VideoCardGpu'},
            'core': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.VideoCardGpuCore']"}),
            'core_count': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.VideoCardGpuCoreCount']"}),
            'default_core_clock': ('django.db.models.fields.IntegerField', [], {}),
            'default_memory_clock': ('django.db.models.fields.IntegerField', [], {}),
            'default_shader_clock': ('django.db.models.fields.IntegerField', [], {}),
            'dx_version': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.VideoCardGpuDirectxVersion']"}),
            'has_multi_gpu_support': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'line': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.VideoCardGpuLine']"}),
            'manufacturing_process': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.VideoCardGpuManufacturingProcess']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'ogl_version': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.VideoCardGpuOpenglVersion']"}),
            'power_conns': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['cotizador.VideoCardHasPowerConnector']", 'null': 'True', 'blank': 'True'}),
            'rops': ('django.db.models.fields.IntegerField', [], {}),
            'stream_processors': ('django.db.models.fields.IntegerField', [], {}),
            'tdmark_06_score': ('django.db.models.fields.IntegerField', [], {}),
            'tdmark_11_score': ('django.db.models.fields.IntegerField', [], {}),
            'tdmark_id': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'tdmark_vantage_score': ('django.db.models.fields.IntegerField', [], {}),
            'tdp': ('django.db.models.fields.IntegerField', [], {}),
            'texture_units': ('django.db.models.fields.IntegerField', [], {}),
            'transistor_count': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.videocardgpuarchitecture': {
            'Meta': {'ordering': "['brand', 'name']", 'object_name': 'VideoCardGpuArchitecture'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.VideoCardGpuBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.videocardgpubrand': {
            'Meta': {'ordering': "['brand']", 'object_name': 'VideoCardGpuBrand'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.videocardgpucore': {
            'Meta': {'ordering': "['family', 'name']", 'object_name': 'VideoCardGpuCore'},
            'family': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.VideoCardGpuCoreFamily']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.videocardgpucorecount': {
            'Meta': {'ordering': "['value']", 'object_name': 'VideoCardGpuCoreCount'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.videocardgpucorefamily': {
            'Meta': {'ordering': "['architecture', 'name']", 'object_name': 'VideoCardGpuCoreFamily'},
            'architecture': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.VideoCardGpuArchitecture']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.videocardgpudirectxversion': {
            'Meta': {'ordering': "['value']", 'object_name': 'VideoCardGpuDirectxVersion'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.DecimalField', [], {'max_digits': '3', 'decimal_places': '1'})
        },
        'cotizador.videocardgpufamily': {
            'Meta': {'ordering': "['brand', 'name']", 'object_name': 'VideoCardGpuFamily'},
            'brand': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.VideoCardGpuBrand']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.videocardgpuline': {
            'Meta': {'ordering': "['family', 'name']", 'object_name': 'VideoCardGpuLine'},
            'family': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.VideoCardGpuFamily']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.videocardgpumanufacturingprocess': {
            'Meta': {'ordering': "['value']", 'object_name': 'VideoCardGpuManufacturingProcess'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.videocardgpuopenglversion': {
            'Meta': {'ordering': "['value']", 'object_name': 'VideoCardGpuOpenglVersion'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.DecimalField', [], {'max_digits': '2', 'decimal_places': '1'})
        },
        'cotizador.videocardhasport': {
            'Meta': {'ordering': "['port', 'quantity']", 'object_name': 'VideoCardHasPort'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'port': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.VideoCardPort']"}),
            'quantity': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.videocardhaspowerconnector': {
            'Meta': {'ordering': "['connector', 'quantity']", 'object_name': 'VideoCardHasPowerConnector'},
            'connector': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.VideoCardPowerConnector']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'quantity': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.videocardmemorybuswidth': {
            'Meta': {'ordering': "['value']", 'object_name': 'VideoCardMemoryBusWidth'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.videocardmemoryquantity': {
            'Meta': {'ordering': "['value']", 'object_name': 'VideoCardMemoryQuantity'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        },
        'cotizador.videocardmemorytype': {
            'Meta': {'ordering': "['name']", 'object_name': 'VideoCardMemoryType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.videocardport': {
            'Meta': {'ordering': "['port']", 'object_name': 'VideoCardPort'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'port': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfaceVideoPort']"})
        },
        'cotizador.videocardpowerconnector': {
            'Meta': {'ordering': "['connector']", 'object_name': 'VideoCardPowerConnector'},
            'connector': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cotizador.InterfacePowerConnector']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cotizador.videocardprofile': {
            'Meta': {'ordering': "['name']", 'object_name': 'VideoCardProfile'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.videocardrefrigeration': {
            'Meta': {'ordering': "['name']", 'object_name': 'VideoCardRefrigeration'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'cotizador.videocardslottype': {
            'Meta': {'ordering': "['value']", 'object_name': 'VideoCardSlotType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'value': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['cotizador']
