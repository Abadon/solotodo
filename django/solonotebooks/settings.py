import os
from os.path import abspath, basename, dirname, join, normpath
from sys import path
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


SITE_NAME = 'apphm'
DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
)

MANAGERS = ADMINS

DATABASES = {
'default': {
'ENGINE': 'django.db.backends.sqlite3',
'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
}
}


TIME_ZONE = 'America/Chicago'
LANGUAGE_CODE = 'en-us'
SITE_ID = 1
USE_I18N = True
USE_L10N = True
MEDIA_ROOT = '/root/solotodo-master/apache/media/pics'
MEDIA_URL = '/media/pics/'
ADMIN_MEDIA_PREFIX = '/media/admin/'
THUMBNAIL_BASEDIR = 'resize'
SECRET_KEY = 'ls!l1u6(#x89ltrbmj)m8*=nxusk5xdv=3x9jpaz^jvh2s@*n9'


FACEBOOK_APP_ID ='1871218339770289'
FACEBOOK_APP_SECRET = '24cfd5d4a84ad131612f000ddeaac9a3'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

LOGIN_URL = '/manager/login/'

ROOT_URLCONF = 'solonotebooks.urls'


TEMPLATE_DIRS = (
os.path.join(BASE_DIR, 'templates'),
)
INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.admin',
    'solonotebooks.cotizador',
#    'south'
)
